<?php
/**
 * Template Name: Template Colecciones BNE.es
 * Esta plantilla se usa para las páginas individuales de nivel 2
 *
 * @author 	Felipe Rodríguez (Serikat)
 * @package @package 	BNE-Theme/single.php
 * @version 1.0
 */
?>

<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>

<div id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>

	<?php while ( have_posts() ) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class( 'post' ); ?>>
			<?php 
			// Imagen Destacada Noticia (post)
			//echo get_the_post_thumbnail( $post->ID, 'full' );
			$imagen_atributos = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
			if ($imagen_atributos) { //si el array no vacío, mostramos la imagen + atributos
			?> 

			<div style="max-width: 100%; min-height: 300px; background:url(<?php echo $imagen_atributos[0]; ?>) center center no-repeat; -webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"></div>
			<?php } ?>

			<div class="post-content" style="margin-top: 50px;">

				<div class="fusion-one-fourth fusion-layout-column campo-izda-noticias">
				<div class="fusion-column-wrapper-lateral-izq">
					<?php 
						// Módulo Noticias: Informacion Práctica
						echo "<h3>";
							if (get_field("tipo-acto-noticias")){ the_field("tipo-acto-noticias");}
						echo "</h3>";

						if (get_field('fecha-noticias')){ the_field('fecha-noticias'); echo "<br />";}		

						if (get_field('horario-noticias')){ the_field('horario-noticias'); echo "<br />";} 

						if( have_rows('enlace-tipo-acto') ):
						 	// loop through the rows of data
						    while ( have_rows('enlace-tipo-acto') ) : the_row();
						        //vars of data
								$texto_enlace = get_sub_field('texto-enlace-tipo-acto');
								$url = get_sub_field('url-enlace-tipo-acto');
								$title_url = get_sub_field('title-url-tipo-acto');
							?>
								<a href="<?php echo $url; ?>" title="<?php echo $title_url; ?>"><?php echo $texto_enlace; ?></a>
							<?php						        
						    endwhile;
						endif;
					?>

					<?php
						// Módulo Noticias: Enlaces Relacionados
						echo "<h3 class='titulo-enlaces-relacionado'>";
							if (get_field("titulo-enlaces-relacionados-noticias")){ the_field("titulo-enlaces-relacionados-noticias"); }
						echo "</h3>";					
						
						$enlaces_relacionados = get_field("enlaces-relacionados-noticias");
						if (!empty($enlaces_relacionados)){
							if( have_rows('enlaces-relacionados-noticias') ):								
							 	// loop through the rows of data
							    while ( have_rows('enlaces-relacionados-noticias') ) : the_row();
							        //vars of data
									$texto_enlace = get_sub_field('texto-enlace-relacionado');
									$url = get_sub_field('url-enlace-relacionado');
									$title_url = get_sub_field('title-url-enlace-relacionado');

									if ($texto_enlace) :
								?>									
									<li class="enlaces-relacionados"><a href="<?php echo $url; ?>" title="<?php echo $title_url; ?>"><?php echo $texto_enlace; ?></a></li>
								<?php
									endif;						        
							    endwhile;
							endif;
						}
					?>
					<br />
					<?php
						// Módulo Noticias: Videos Relacionados					
						if (get_field("videos-relacionados-noticias")) :
							the_field("videos-relacionados-noticias");
						endif;

					?>
				<div class="fusion-clearfix"></div>
				</div></div>


				<div class="fusion-three-fourth fusion-layout-column fusion-column-last fusion-spacing-yes">
					<div class="fusion-column-wrapper"><?php echo get_the_date(); ?><h1 class="titulo-noticia"><?php the_title() ?></h1><div class="contenido-noticia"><?php the_content(); ?></div>
					<div class="fusion-clearfix"></div>
					</div>
				</div>

				<?php //avada_link_pages(); ?>
			</div>
				<?php
				/**
				 * Render Related Posts
				 */
				echo avada_render_related_posts( get_post_type() );
				?>
		</article>
	<?php endwhile; ?>
	<?php wp_reset_query(); ?>
</div>
<?php do_action( 'avada_after_content' ); ?>
<?php get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
