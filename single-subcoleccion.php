<?php
/**
 * Template Name: Template Subcolecciones BNE.es
 * Esta plantilla se usa para la página Subcolecciones (CPT subcoleccion)
 *
 * @author 	Felipe Rodríguez (Serikat)
 * @package @package BNE-Theme/single-subcoleccion.php
 * @version 1.0
 */

?>

<?php get_header(); ?>


<script type="text/javascript">
$('article').readmore({
  speed: 75,
  moreLink: '<button class="btn-custom">LEER TEXTO COMPLETO DE LA SUBCOLECCIÓN</button>',
  lessLink: '<button class="btn-custom">LEER MENOS</button>'
  afterToggle: function(trigger, element, expanded) {
    if(! expanded) { // The "Close" link was clicked
      $('html, body').animate( { scrollTop: element.offset().top }, {duration: 100 } );
    }
  }
});
var moreText = "Leer más",
    lessText = "Leer menos",
    moreButton = $("button.btn-custom");
moreButton.click(function () {
    var $this = $(this);
    $this.text($this.text() == moreText ? lessText : moreText).next(".content-expand").slideToggle("fast");
});
</script>
	<div id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>
		<div id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
					
					<?php if( has_post_thumbnail() && get_post_meta( $post->ID, 'pyre_show_first_featured_image', true ) != 'yes' ): ?>
					<?php $attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
					<?php $full_image = ''; ?> 
					<?php $full_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
					<?php $attachment_data = wp_get_attachment_metadata(get_post_thumbnail_id()); ?>
					
					<?php //Muestra la imagen de fondo de la subcolección si hay
					if (!empty($attachment_image[0])){ ?>
					<div id="imagezoom" style="max-width: 100%; min-height: 300px; background:url(<?php echo $attachment_image[0]; ?>) center center no-repeat; -webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;">		
						<h1 class="coleccion-title"><?php the_title(); ?></h1>					
					</div>
					<?php // si no hay imagen, fondo negro sobre letra blanca
					}else{ ?>
					<div style="max-width: 100%; min-height: 300px; background-color: #000; -webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;">		
						<h1 class="coleccion-title"><?php the_title(); ?></h1>					
					</div>
					<?php } ?>

					<?php endif; ?>

			<div class="post-content"><br />
				<div class="contenido-texto">	
					<?php the_content(); ?>
				</div>
			<?php // Toogle Leer Mas para single SubColeccion 
			// Siempre y cuando esté relleno el custom field llamado "leer-mas-subcoleccion"
			// Se muestra el botón + efecto fade
			// Si está vacío, NO se muestra el botón
			if (get_field("leer-mas-subcoleccion")){ ?>
				<div class="content-expand" style="display: none;">
					<div class="contenido-texto">
						<?php the_field("leer-mas-subcoleccion"); ?>
					</div>
				</div>
				<button class="btn-custom">LEER TEXTO COMPLETO DE LA SUBCOLECCIÓN</button>
			<?php } ?>
			</div>

<?php
	/*
	* Inicializamos los objetos posts, para hacer uso del Módulo ACF obras.
	* En este módulo, tiene campos multiselección. Usamos set_postdata para hacer uso
	* de las funciones de WP core nativas de forma normal.
	* http://codex.wordpress.org/Template_Tags/get_posts#Reset_after_Postlists_with_offset
	*/

	$post_objects = get_field('obra-asociada-subcoleccion'); //Llamada al módulo para inicializarlo

	if( $post_objects ): ?>
		<div class="container-obras">
		<h4 class="otras-colecciones">Obras</h4>
		    <div class="row">
		        <?php 
		        $i = 1; //contador para controlar el numero de obras a mostrar
		        foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
        			<?php setup_postdata($post); //preparamos los datos de las obras asociadas ?>

        			<?php 
        			// Como máximo, se muestran 7 obras asociadas a la colección
        			if ($i<=7) { ?>
						<?php if( has_post_thumbnail() ): 
							$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); 
						?>
						<?php //Muestra la imagen de fondo de la Obra si existe + el titulo obra sobre ella
						if (!empty($attachment_image[0])){ ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title();?> - <?php bloginfo ('name'); ?>"><div class="col-md-3 obra" style="max-width: 100%; min-height: 300px; background:url(<?php echo $attachment_image[0]; ?>) center center no-repeat; -webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover; border: 0.01em solid #fff; padding: 100px 0 48px 0;">		
								<p class="text-hidden"><?php the_title(); ?></p>
							</div></a>
						<?php // si no hay imagen, fondo negro sobre letra blanca + el titulo obra sobre ella
						}else{ ?>
							<div class="col-md-3" style="max-width: 100%; min-height: 300px; background-color: #1D1D1D; -webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover; border: 0.01em solid #fff; padding: 100px 0 48px 0;">		
								<a href="<?php the_permalink(); ?>" title="<?php the_title();?> - <?php bloginfo ('name'); ?>"></a>					
							</div>
					<?php } 
						endif; 
					$i++;	//incrementamos el contador
					}else{
						break; //si hay 8 o más obras, nos salimos del bucle para evitar mostrar más
					}
						?>
    			<?php endforeach; 
				wp_reset_postdata(); // Resetamos el objeto $post para que funciona bien la pagina
    			?>
    			<div class="col-md-3" style="text-align:center; max-width: 100%; min-height: 300px; background-color: #1D1D1D; -webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover; border: 0.01em solid #fff; padding: 100px 0 48px 0;">
    			<?php 
    			/* Módulo Colección: URL a obra en BDH
    			* Desde el panel de control, se controla la URL 
    			* a la que apunta "Ver más en BDH".
    			*/
    			if (get_field("url-obra-bdh")){ ?>
    				<a style="color: #fff;" target="_blank" href="<?php the_field("url-obra-bdh"); ?>" title="Ver más en BDH de la SubColección: <?php the_title(); ?>">Ver más en BDH <i class="fa fa-plus-circle fa-5" aria-hidden="true"></i>
    				</a>
    			<?php } ?>
    			</div>
    		</div>
    	</div>     	
<?php endif; ?>

<?php
	/*
	* Inicializamos los objetos posts, para hacer uso del Módulo ACF Subcolecciones.
	* En este módulo, tiene campos multiselección. Usamos set_postdata para hacer uso
	* de las funciones de WP core nativas de forma normal.
	* http://codex.wordpress.org/Template_Tags/get_posts#Reset_after_Postlists_with_offset
	*/

	$post_objects = get_field('subcoleccion-destacadas'); //Llamada al módulo para inicializarlo

	if( $post_objects ): ?>
		<div class="container-obras">
		<h4 class="otras-colecciones">Subcolecciones</h4>
		    <div class="row">
		        <?php 
		        $i = 1; //contador para controlar el numero de subcolecciones a mostrar
		        foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
        			<?php setup_postdata($post); //preparamos los datos de las subcoleciones asociadas ?>

        			<?php 
        			// Como máximo, se muestran 8 subcolecciones asociadas a la colección
        			if ($i<=8) { ?>
						<?php if( has_post_thumbnail() ): 
							$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); 
						?>
						<?php //Muestra la imagen de fondo de la Obra si existe + el titulo obra sobre ella
						if (!empty($attachment_image[0])){ ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title();?> - <?php bloginfo ('name'); ?>"><div class="col-md-3 obra" style="max-width: 100%; min-height: 300px; background:url(<?php echo $attachment_image[0]; ?>) center center no-repeat; -webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover; border: 0.01em solid #fff; padding: 100px 0 48px 0;">		
								<p class="text-hidden"><?php the_title(); ?></p>
							</div></a>
						<?php // si no hay imagen, fondo negro sobre letra blanca + el titulo obra sobre ella
						}else{ ?>
							<div class="col-md-3" style="max-width: 100%; min-height: 300px; background-color: #1D1D1D; -webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover; border: 0.01em solid #fff; padding: 100px 0 48px 0;">		
								<a href="<?php the_permalink(); ?>" title="<?php the_title();?> - <?php bloginfo ('name'); ?>"><?php the_title(); ?></a>					
							</div>
					<?php } 
						endif; 
					$i++;	//incrementamos el contador
					}else{
						break; //si hay 9 o más subcolecciones, nos salimos del bucle para evitar mostrar más
					}
						?>
    			<?php endforeach; 
				wp_reset_postdata(); // Resetamos el objeto $post para que funciona bien la pagina
    			?>
    		</div>
    	</div>     	
<?php endif; ?>

	</div>
	</div>
<?php wp_reset_query(); ?>	
<?php get_footer();
// Omit closing PHP tag to avoid "Headers already sent" issues.