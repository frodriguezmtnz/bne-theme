<?php
/**
 * Template Name: Template Obras BNE.es
 * Este plantilla se usa para la página Obras (CPT obra)
 *
 * @author 	Felipe Rodríguez (Serikat)
 * @package @package 	BNE-Theme/single-obra.php
 * @version 1.0
 */
?>

<?php get_header(); ?>
	<div id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>
		<div id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
					<?php if( has_post_thumbnail() && get_post_meta( $post->ID, 'pyre_show_first_featured_image', true ) != 'yes' ): ?>
					<?php $attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
					<?php $full_image = ''; ?> 
					<?php $full_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
					<?php $attachment_data = wp_get_attachment_metadata(get_post_thumbnail_id()); ?>
					<div style="max-width: 100%; min-height: 300px; background:url(<?php echo $attachment_image[0]; ?>) center center no-repeat; -webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;">
					</div>
					<?php endif; ?>
			<div class="post-content" style="margin-top: 30px;">
				<div class="fusion-one-fourth fusion-layout-column fusion-spacing-yes campo-izda-noticias">
				<div class="fusion-column-wrapper">
					<h2>FICHA TÉCNICA</h2>
					<?php // Titulo Obra
					if (get_field("titulo-obra")){ ?>					
					<h3 class="formato-ficha-tecnica-obra">Título:</h3>
						<p class="obra-ficha-tecnica">
							<?php the_field("titulo-obra"); } ?>
						</p>
					<?php // Autor Obra
					if( have_rows('autor-obra') ): ?>
					<h3 class="formato-ficha-tecnica-obra">Autor:</h3>
					<?php
					    while ( have_rows('autor-obra') ) : the_row();
							$texto_enlace = get_sub_field('autor-nombre-obra');
							$title_url = get_sub_field('autor-nombre-obra');
							$url = get_sub_field('enlace-autor-bdh-obra');
							if ($url){
						?>							
							<a class="enlace-ficha-tecnica-obra" href="<?php echo $url; ?>" title="Autor de la Obra: <?php echo $title_url; ?>"><?php echo $texto_enlace; ?></a> &raquo;
						<?php } else { ?>
							<p class="obra-ficha-tecnica">
							<?php echo $texto_enlace; } ?>
							</p>
						<?php endwhile;
					endif;
					?>
					<?php // Fecha publicación Obra
					if (get_field("fecha-publicacion-obra")){ ?>					
					<h3 class="formato-ficha-tecnica-obra">Fecha de publicación:</h3>
						<p class="obra-ficha-tecnica">
							<?php the_field("fecha-publicacion-obra"); } ?>
						</p>
					<?php // Tipo de Material Obra
					if (get_field("tipo-material-obra")){ ?>						
					<h3 class="formato-ficha-tecnica-obra">Tipo de Material:</h3>
						<p class="obra-ficha-tecnica">
							<?php the_field("tipo-material-obra"); } ?>
						</p>
					<?php // Descripción Física Obra
					if (get_field("descripcion-fisica-obra")){ ?>
					<h3 class="formato-ficha-tecnica-obra">Descripción física:</h3>
						<p class="obra-ficha-tecnica">
							<?php the_field("descripcion-fisica-obra"); } ?>
						</p>
					<?php // Signatura Obra
					if (get_field("signatura-obra")){ ?>						
					<h3 class="formato-ficha-tecnica-obra">Signatura:</h3>
						<p class="obra-ficha-tecnica">
							<?php the_field("signatura-obra"); } ?>
						</p>
					<?php // Enlace al Acceso Documento Digitalizado (Obra)
						if (get_field("enlace-acceso-documento-digitalizado-obra")){  ?>
						<a class="enlace-ficha-tecnica-obra" href="<?php echo the_field("enlace-acceso-documento-digitalizado-obra"); ?>" title="Acceso al documento digitalizado de la Obra '<?php the_title() ?>'"><h3>Documento Digitalizado &raquo;</h3></a>
					<?php } ?>
					<?php // Enlace Registro en el Catálogo (Obra)
						if (get_field("enlace-registro-catalogo-obra")){ ?>
						<a class="enlace-ficha-tecnica-obra" href="<?php echo the_field("enlace-registro-catalogo-obra"); ?>" title="Ver registro en el catálogo de la Obra '<?php the_title() ?>'"><h3>Registro en el Catálogo &raquo;</h3></a>
					<?php } ?>
						<div class="fusion-clearfix"></div>
				</div></div>
				<div class="fusion-three-fourth fusion-layout-column fusion-column-last fusion-spacing-yes">
					<div class="fusion-column-wrapper">
						<?php //echo get_the_date(); ?>
						<h1 class="titulo-noticia">
							<?php the_title() ?>
						</h1>
						<div class="contenido-texto">
							<?php the_content(); ?>
						</div>
						<div class="fusion-clearfix"></div>
					</div>
				</div>
			</div>		
	</div>
<?php get_footer();
// Omit closing PHP tag to avoid "Headers already sent" issues.