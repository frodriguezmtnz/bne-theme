<?php
/**
 * Template Name: Servicios BNE (nivel 2)
 * Este plantilla se usa para los servicios adicionales que tiene la BNE.es (sub-servicios) de nivel 2 de click.
 */

?>

<?php

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>
<div id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>
	<?php while ( have_posts() ) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								<?php if ( has_post_thumbnail() && 'yes' != get_post_meta( $post->ID, 'pyre_show_first_featured_image', true ) ) : ?>
									<?php $attachment_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
									<?php $full_image       = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
									<?php $attachment_data  = wp_get_attachment_metadata( get_post_thumbnail_id() ); ?>
									<div style="max-width: 100%; min-height: 300px; background:url(<?php echo $attachment_image[0]; ?>) center center no-repeat; -webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;">						
										<h1 class="coleccion-title"><?php the_title(); ?></h1>					
									</div>
								<?php endif; ?>
			<?php fusion_breadcrumbs(); ?>
			<div class="post-content">
				<div class="contenido-texto">
					<?php the_content(); ?>
				</div>
				<?php //avada_link_pages(); ?>
			</div>
		</div>
	<?php endwhile; ?>
	<?php wp_reset_query(); ?>
</div>
<?php //do_action( 'avada_after_content' ); ?>
<?php get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */