<?php
/**
 * Template Name: Template Related Posts for BNE.es
 * Esta plantilla se usa para las single post para Noticias bne.es
 *
 * @author 	Felipe Rodríguez (Serikat)
 * @package @package 	BNE-Theme/templates/related-posts.php
 * @version 1.0
 */
?>

<?php

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>

<?php 
// Inicializacion para libreria para detectar el user-agent de movil/tablet
require_once 'bne/mobile-detect/Mobile_Detect.php';
$detect = new Mobile_Detect;
?>

<div class="related-posts single-related-posts">

<div class="fusion-one-fourth fusion-layout-column fusion-spacing-no" style="margin-top:0px;margin-bottom:0px;"><div class="fusion-column-wrapper-noticias"><div class="fusion-content-boxes content-boxes columns fusion-columns-1 fusion-columns-total-1 fusion-content-boxes-1 content-boxes-clean-horizontal row content-left content-boxes-icon-on-side" data-animationoffset="100%" style="margin-top:14%;margin-bottom:0px;"><style type="text/css" scoped="scoped">.fusion-content-boxes-1 .heading h2{color:#7A7A7A;}
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading h2,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .heading-link h2,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading h2,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .heading-link h2,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::after,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::before,
			.fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover:after,
			.fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover:before,
			.fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::after,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::before,			
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .icon .circle-no,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .icon .circle-no {
				color: #9e9e9e !important;
			}
			.fusion-content-boxes-1 .fusion-content-box-hover .heading-link:hover .icon i.circle-yes,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box:hover .heading-link .icon i.circle-yes,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon i.circle-yes,
			.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .icon i.circle-yes {
				background-color: #9e9e9e !important;
				border-color: #9e9e9e !important;
			}</style><div style="border-color:#333333;" class="fusion-column content-box-column content-box-column-1 col-lg-12 col-md-12 col-sm-12 fusion-content-box-hover content-box-column-last content-box-column-last-in-row"><div class="col content-wrapper link-area-box link-type-text icon-hover-animation-fade" style="background-color: transparent; min-height: 145px; height: auto; overflow: visible;" data-link="http://localhost/wp-bne/esp/noticias/"><div class="heading icon-left"><a class="heading-link" href="http://10.6.217.104/noticias/" target="_self"><h2 class="content-box-heading" style="color: #7A7A7A; font-size: 30px;line-height:35px;" data-inline-fontsize="true" data-inline-lineheight="true" data-fontsize="30" data-lineheight="35">Volver a las noticias</h2></a></div><div class="fusion-clearfix"></div><div class="content-container"></div></div></div><div class="fusion-clearfix"></div><div class="fusion-clearfix"></div></div><div class="fusion-clearfix"></div></div></div>

<div class="fusion-three-fourth fusion-layout-column fusion-column-last fusion-spacing-no" style="margin-top:0px;margin-bottom:0px;">
	<div class="fusion-column-wrapper" style="border:1px solid #eaeaea;">
		<div class="fusion-column-table">
			<div class="fusion-column-tablecell">
				<div class="fusion-recent-posts avada-container layout-default layout-columns-3">
					<section class="fusion-columns columns fusion-columns-3 columns-3">
					<?php
					/**
					 * Loop through related posts.
					 */
					$i = 0;
					?>
					<?php while ( $related_posts->have_posts() && $i < 3) : $related_posts->the_post();?>

						<div class="fusion-column column col col-lg-4 col-md-4 col-sm-4">
							<div class="recent-posts-content">
							<h4 class="entry-title" data-fontsize="30" data-lineheight="27">
								<a href="<?php echo get_permalink( get_the_ID() ); ?>"_self><?php // *Imprime el Titulo de la noticia segun el dispositivo
									// Si es version movil/tablet, recortamos el titulo a 30 caracteres.
									if ( $detect->isMobile() || $detect->isTablet() ){
										the_title_recortado ('', '...', true, '30');
									}else{									
										the_title_recortado ('', '...', true, '45');
									}?></a></h4>
							</div>
							<div class="recent-posts-content-resumen"><?php
							// *Imprime el resumen manual o del contenido segun el dispositivo 
							if ( $detect->isMobile() || $detect->isTablet() ){
								// version móvil
								if (has_excerpt()) { //extracto manual
									echo limitar_palabras(get_the_excerpt(), '6');
								}else { //extracto auto del content recortado
									echo limitar_palabras(get_the_excerpt(), '5');	
								} 
							}else{ //version desktop
								if (has_excerpt()) { //extracto manual
									echo limitar_palabras(get_the_excerpt(), '8');			
								}else { //extracto auto del content recortado
									echo limitar_palabras(get_the_excerpt(), '6');	
								}
							}
							?>
							</div>
						</div>						
		
					<?php $i++; endwhile; ?>

					</section>
				</div>
				<div class="fusion-clearfix"></div>
			</div>
		</div>
	</div>
</div>


<?php wp_reset_postdata();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
