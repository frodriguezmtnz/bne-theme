<?php
/**
 * Template Name: Sección Layout Grid BNE (Inicio BNE)
 * Esta plantilla se usa como modulo del template (BNE-Theme/inicio.php) 
 *   Carga los campos personalizados editables desde el panel de control de WP,
 *   más el diseño y programación para este módulo.
 *
 * @author 		Felipe Rodríguez (Serikat)
 * @package 	BNE-Theme/templates/bne/layout-noticias.php
 * @version     1.0
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) { exit( 'Direct script access denied.' ); }

// Inicializacion para libreria para detectar el user-agent de movil/tablet
require_once 'mobile-detect/Mobile_Detect.php';
$detect = new Mobile_Detect;

	global $post;

	// The query arguments: https://codex.wordpress.org/Template_Tags/get_posts
	$args = array(
	    'post_type' => 'post',
	    'posts_per_page' => 6,
	    'category_name' => 'noticias',
	    'order' => 'DESC',
	    'orderby' => 'date', // 'orderby' => 'rand',
	    'post_status' => 'publish',
	    'post' => array( $post->ID )
	);

	// Create the related query
	$rel_query = new WP_Query( $args );

	// Check if there are some post news
	if( $rel_query->have_posts() ) : ?>

	
	<?php // The Loop for news + design
		while ( $rel_query->have_posts() ) :
	    	$rel_query->the_post();
	?>

		<article id="post" class="fusion-post-grid post post type-post status-publish format-standard has-post-thumbnail hentry category-noticias">
		<a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
			<div class="fusion-post-wrapper hover-type-liftup">
				<div class="fusion-post-content-wrapper">
					<div class="fusion-post-content post-content">
						<h2 class="blog-shortcode-post-title entry-title" data-fontsize="30" data-lineheight="27">
							<?php 
								//if (isMobile()){ funcion antigua que tira del "functions.php"
								// Si es version movil/tablet, recortamos el titulo a 30 caracteres.
								if ( $detect->isMobile() || $detect->isTablet() ){
									the_title_recortado ('', '...', true, '30');
									//echo "FUNCIONA";
								}else{
									the_title();
								}								
							?>
						</h2>
						<div class="fusion-post-content-container"><?php echo the_excerpt(); ?></div>
					</div>
				</div>
				<div class="fusion-clearfix"></div>
			</div>
		</a>
		</article> 
<?php
		endwhile;
 	endif;
?>
</div>

<?php echo do_shortcode ("[ajax_load_more id='cargar-mas-noticias' container_type='div' post_type='post' posts_per_page='4' category='noticias' pause='true' transition='fade' button_label='Cargar más noticias' button_loading_label='Cargando el resto de noticias...']");


//echo do_shortcode ("[ajax_load_more id="cargar-mas-noticias" container_type="div" post_type="post" posts_per_page="4" category="noticias" pause="true" transition="fade" button_label="Cargar más noticias" button_loading_label="Cargando el resto de noticias..."]");
	//echo do_shortcode ("[ajax_load_more id='cargar-mas-noticias' container_type='div' post_type='post' posts_per_page='4' category='noticias' pause='true' transition='fade' button_label='Cargar más noticias' button_loading_label='Cargando el resto de noticias...']");
?>



<?php
// If infinite scroll with "load more" button is used.
//if ( Avada()->settings->get( 'blog_pagination_type' ) == 'load_more_button' ) {
	//echo '<div class="fusion-load-more-button fusion-blog-button fusion-clearfix">' . apply_filters( 'avada_load_more_posts_name', esc_attr__( 'Cargar más noticias', 'Avada' ) ) . '</div>';
//}

// Get the pagination.
fusion_pagination( $pages = '', $range = 2 );

?>


<div class="fusion-clearfix"></div>


<?php
	// Reset the query wp
	wp_reset_query();
/* Omit closing PHP tag to avoid "Headers already sent" issues. */	