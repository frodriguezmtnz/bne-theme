<?php
/**
 * Template Name: Seccion BNE-Labs (Inicio BNE)
 * Esta plantilla se usa como modulo del template (BNE-Theme/inicio.php) 
 *   Carga los campos personalizados editables desde el panel de control de WP,
 *   más el diseño y programación para este módulo.
 *
 * @author 		Felipe Rodríguez (Serikat)
 * @package 	BNE-Theme/templates/bne/bnelabs-inicio.php
 * @version     1.0
 */
?>

<?php
/**
 * Template Name: Template Colecciones BNE BNE.es
 * Este plantilla se usa para la página Colecciones (CPT coleccion)
 *
 * @author 	Felipe Rodríguez (Serikat)
 * @package @package 	BNE-Theme/single-coleccion.php
 * @version 1.0
 */

?>

	<div class="fusion-fullwidth fullwidth-box fusion-fullwidth-2  fusion-parallax-none hundred-percent-fullwidth" 
	style=" border-color: #eae9e9;
	    	border-bottom-width: 0px;
		    border-top-width: 0px;
		    border-bottom-style: solid;
		    border-top-style: solid;
		    padding-bottom: 0px;
		    padding-top: 0px;
		    background-position: left top;
		    background-repeat: no-repeat;
		    -webkit-background-size: cover;
		    -moz-background-size: cover;
		    -o-background-size: cover;
		    background-size: cover;
		    background-image: url(<?php // Carga la Imagen del fondo (background) del campo bnelabs
		    			if(get_field("imagen-fondo-bnelabs-inicio")) {
		    				the_field("imagen-fondo-bnelabs-inicio");  
		    			} else {
		    				//echo "/wp-content/themes/bne-theme/images/bnelabs-imagen-fondo.jpg";
		    				echo get_stylesheet_directory_uri().'/images/bnelabs-imagen-fondo.jpg';
		    			} 
		    		?> );
			">

		<div class="fusion-row" style="background-repeat: no-repeat; background-position: center center; filter: drop-shadow(0px 0px 5px #5e5e5e); background-image: url(<?php // Carga el Icono superpuesto sobre la Imagen del fondo (background) del campo bnelabs

	    			// Se puede forzar a mostrar otro icono antes de seleccionar del listado
	    			if(get_field("icono-imagen-fondo-bnelabs-inicio")) {
	    				the_field("icono-imagen-fondo-bnelabs-inicio");  
	    				// Si no hay ningun icono subido, se muestra el icono
	    				// seleccionado del listado.
	    			} elseif (get_field("listado-iconos-imagen-fondo-bnelabs-inicio")) {
	    				the_field("listado-iconos-imagen-fondo-bnelabs-inicio"); 
	    				// Si falla algun modulo anterior al cargar, se muestra un icono fijo
	    				}else { echo "http://emojipedia-us.s3.amazonaws.com/cache/92/f3/92f3862b077594a837d1d573cc86cc21.png"; }
	    		?> );
    		">
			<div class="fusion-two-third fusion-layout-column fusion-spacing-no">
			<div class="fusion-column-wrapper">
				<div class="fusion-column-table">				
					<div class="fusion-column-tablecell">
						<div class="fusion-clearfix"></div></div>
					</div>
				</div>
			</div>
			<div class="fusion-one-third fusion-layout-column fusion-column-last fusion-spacing-no columna-museo-derecha-transparencia">
				<div class="fusion-column-wrapper">
					<div class="fusion-column-table">
						<div class="fusion-column-tablecell">
							<div class="imageframe-align-center-bnelabs">
								<span class="fusion-imageframe imageframe-none imageframe-2 hover-type-none">	BNE LABS
								</span>
							</div>
						<h1 class="h1-inicio-museo" data-fontsize="45" data-lineheight="48">
							<span><?php 
								// Carga el Titulo del Módulo BNE-LABS HOME
				    			if(get_field("campo-bnelabs-titulo-inicio")) {
				    				the_field("campo-bnelabs-titulo-inicio");  
				    			} else {
				    				echo "Chef BNE";
				    			} 
				    		?></span><br>
						</h1>
						<div class="centrar-texto-bnelabs"><?php 								
								// Carga el Descripción del Módulo BNE-LABS HOME
								if (get_field("campo-bnelabs-inicio") ) {
									the_field("campo-bnelabs-inicio");									
								}else{ //si no, carga el contenido por defecto aquí.
									echo "<p>Un breve recorrido por la gastronomía de España a través de sus recetarios antiguos, manuales y tratados. 12 vídeos que ponen en contexto la historia de una cocina, el mestizaje de sus ingredientes y su evolución.</p>"; 
								}
								echo "<br /><br />";
							?>
						</div>
						<div class="fusion-clearfix"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="fusion-clearfix"></div>

			<div class="fusion-two-third fusion-layout-column fusion-spacing-no">
				<div class="fusion-column-wrapper">
					<div class="fusion-column-table">
						<div class="fusion-column-tablecell"><div class="fusion-clearfix"></div></div>
					</div></div>
				</div>
				<div class="fusion-one-third fusion-layout-column fusion-column-last fusion-spacing-no columna-museo-derecha-transparencia">
					<div class="fusion-column-wrapper bordesup-visitar-museo">
						<div class="fusion-column-table">
							<div class="fusion-column-tablecell">
								<h2 class="h2-inicio-museo"><a class="h2-enlace-inicio-museo" title="<?php 
										// Campo TITLE para el Módulo BNE-LABS HOME
										if (get_field('campo-texto-verproyecto-bnelabs-inicio') ) {
											the_field('campo-texto-verproyecto-bnelabs-inicio');
										}else{ 
											echo "Reservar Visita Guiada en la Biblioteca Nacional de España"; 
										}
									?> - Biblioteca Nacional de España" href="<?php 
										// Campo ENLACE para el Módulo BNE-LABS HOME
										if (get_field('campo-enlace-verproyecto-bnelabs-inicio') ) {
											the_field('campo-enlace-verproyecto-bnelabs-inicio');
										}else{ 
											//echo "/proyecto-default-bnelabs/"; 
										}
									?>" ><?php								
									// Carga el Texto "Ver Proyecto" del Módulo BNE-LABS HOME
									if (get_field("campo-texto-verproyecto-bnelabs-inicio") ) {
										the_field("campo-texto-verproyecto-bnelabs-inicio");	
									}else{ //si no, carga el contenido por defecto aquí.
										echo "Ver Proyecto"; 
									}?> &raquo;</a></h2>
								<div class="fusion-clearfix"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="fusion-clearfix"></div>

				<div class="fusion-two-third fusion-layout-column fusion-spacing-no">
					<div class="fusion-column-wrapper">
						<div class="fusion-column-table">
							<div class="fusion-column-tablecell">
								<div class="fusion-clearfix"></div>
							</div>
						</div>
					</div>
				</div>

				<div class="fusion-one-third fusion-layout-column fusion-column-last fusion-spacing-no columna-museo-derecha-transparencia">
					<div class="fusion-column-wrapper bordesup-visitar-museo">
						<div class="fusion-column-table">
							<div class="fusion-column-tablecell">
								<h2 class="h2-inicio-museo">
									<a class="h2-enlace-inicio-museo" title="Ver web de BNE LABS - Biblioteca Nacional de España" href="#">VER BNE LABS &raquo;</a>
								</h2>
							<div class="fusion-clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			<div class="fusion-clearfix"></div>
		</div>
	</div>