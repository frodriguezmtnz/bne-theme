<?php
/**
 * Template Name: Sección Bibliotecarios BNE (Inicio BNE)
 * Esta plantilla se usa como modulo del template (BNE-Theme/inicio.php) 
 *   Carga los campos personalizados editables desde el panel de control de WP,
 *   más el diseño y programación para este módulo.
 *
 * @author 		Felipe Rodríguez (Serikat)
 * @package 	BNE-Theme/templates/bne/bibliotecarios-inicio.php
 * @version     1.0
 */
?>
	<div class="fusion-fullwidth fullwidth-box fusion-fullwidth-2  fusion-parallax-none hundred-percent-fullwidth" style="    
    border-color: #eae9e9;
    border-bottom-width: 0px;
    border-top-width: 0px;
    border-bottom-style: solid;
    border-top-style: solid;
    padding-bottom: 0px;
    padding-top: 0px;
    background-position: left top;
    background-repeat: no-repeat;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    background-image: url(<?php 
    			if(get_field("campo-fondo-bibliotecarios-inicio")) {
    				the_field("campo-fondo-bibliotecarios-inicio");  
    			} else {
    				echo get_stylesheet_directory_uri().'/images/fondo-bibliotecarios-bne.jpg';
    			} 
    		?> );
	">
		<div class="fusion-row">
			<div class="fusion-two-third fusion-layout-column fusion-spacing-no">
			<div class="fusion-column-wrapper">
				<div class="fusion-column-table">
					<div class="fusion-column-tablecell">
						<div class="fusion-clearfix"></div></div>
					</div>
				</div>
			</div>
			<div class="fusion-one-third fusion-layout-column fusion-column-last fusion-spacing-no columna-derecha-modulo-transparencia">
				<div class="fusion-column-wrapper">
					<div class="fusion-column-table">
						<div class="fusion-column-tablecell">
						<h1 class="h1-inicio-museo" data-fontsize="45" data-lineheight="48">
							<span><?php 								
									// Carga el TITULO de "Bibliotecarios BNE" (HOME)
									if (get_field("campo-titulo-bibliotecarios-inicio") ) {
										the_field("campo-titulo-bibliotecarios-inicio");
									}else{ 
										echo "Bibliotecarios <br />BNE"; 
									}
								?></span>
						</h1>
						<div class="centrar-texto"><?php 								
								// Carga el texto descripcion de "Bibliotecarios BNE" (HOME)
								if (get_field("campo-descripcion-bibliotecarios-inicio") ) {
									the_field("campo-descripcion-bibliotecarios-inicio");
								}else{ 
									echo "<p class='transparencia-bne'>Bibliotecarios Lorem ipsum dolor sit amet, hinc scaevola signiferumque ut sea, omnesque accusata id ius, te vel nihil tollit. Quod elit velit sea cu, et inani animal vel. Nobis efficiendi necessitatibus vim id, vel cu ignota possit eirmod.</p>";
									echo "<br />";
								}
							?>
						</div>
						<div class="fusion-clearfix"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="fusion-clearfix"></div>

			<div class="fusion-two-third fusion-layout-column fusion-spacing-no">
				<div class="fusion-column-wrapper">
					<div class="fusion-column-table">
						<div class="fusion-column-tablecell"><div class="fusion-clearfix"></div></div>
					</div></div>
				</div>
				<div class="fusion-one-third fusion-layout-column fusion-column-last fusion-spacing-no columna-derecha-modulo-transparencia">
				<?php if( have_rows('campo-url-bibliotecarios-inicio') ): ?>
					<?php while( have_rows('campo-url-bibliotecarios-inicio') ): the_row(); ?> 
					<div class="fusion-column-wrapper bordesup-visitar-museo">								
							<div class="fusion-column-table">
								<div class="fusion-column-tablecell">
									<h2 class="h2-inicio-museo">
									<?php
										// variables
										$texto_enlace 	= get_sub_field('titulo-url-bibliotecarios');
										$enlace 		= get_sub_field('url-bibliotecarios');
										$title_url 		= get_sub_field('title-url-bibliotecarios');
									?>			
									<a class="h2-enlace-inicio-museo" title="<?php echo $title_url; ?>" href="<?php echo $enlace; ?>"><?php echo $texto_enlace; ?> &raquo;</a></h2>
								<div class="fusion-clearfix"></div>
							</div>
						</div>			
					</div>
					<?php endwhile; ?>
				<?php endif; ?>					
				</div>
				<div class="fusion-clearfix"></div>

				<div class="fusion-two-third fusion-layout-column fusion-spacing-no">
					<div class="fusion-column-wrapper">
						<div class="fusion-column-table">
							<div class="fusion-column-tablecell">
								<div class="fusion-clearfix"></div>
							</div>
						</div>
					</div>
				</div>
		</div>
	</div>