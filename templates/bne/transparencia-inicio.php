<?php
/**
 * Template Name: Sección Transparencia BNE (Inicio BNE)
 * Esta plantilla se usa como modulo del template (BNE-Theme/inicio.php) 
 *   Carga los campos personalizados editables desde el panel de control de WP,
 *   más el diseño y programación para este módulo.
 *
 * @author 		Felipe Rodríguez (Serikat)
 * @package 	BNE-Theme/templates/bne/transparencia-inicio.php
 * @version     1.0
 */
?>
	<div class="fusion-fullwidth fullwidth-box fusion-fullwidth-2  fusion-parallax-none hundred-percent-fullwidth" style="    
    border-color: #eae9e9;
    border-bottom-width: 0px;
    border-top-width: 0px;
    border-bottom-style: solid;
    border-top-style: solid;
    padding-bottom: 0px;
    padding-top: 0px;
    background-position: left top;
    background-repeat: no-repeat;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    background-image: url(<?php 
    			if(get_field("campo-fondo-transparencia-inicio")) {
    				the_field("campo-fondo-transparencia-inicio");  
    			} else {
    				echo get_stylesheet_directory_uri().'/images/fondo-transparencia-bne.jpg';
    			} 
    		?> );
	">
		<div class="fusion-row">
			<div class="fusion-two-third fusion-layout-column fusion-spacing-no">
			<div class="fusion-column-wrapper">
				<div class="fusion-column-table">
					<div class="fusion-column-tablecell">
						<div class="fusion-clearfix"></div></div>
					</div>
				</div>
			</div>
			<div class="fusion-one-third fusion-layout-column fusion-column-last fusion-spacing-no columna-derecha-modulo-transparencia">
				<div class="fusion-column-wrapper">
					<div class="fusion-column-table">
						<div class="fusion-column-tablecell">
							<div class="imageframe-align-center">
								<span class="fusion-imageframe imageframe-none imageframe-2 hover-type-none"><img src="<?php 

									if(get_field("campo-transparencia-logo-inicio")) {
    									the_field("campo-transparencia-logo-inicio");  
    								} else {
										echo get_stylesheet_directory_uri().'/images/logo-bne-transparente.png';						
    								}
								?>" alt="<?php 
									// Carga la etiqueta ALT del logo de "Transparencia BNE" (HOME)
									if(the_field("campo-transparencia-logo-inicio") && get_field("campo-alt-transparencia-inicio")) {
    									the_field("campo-alt-transparencia-inicio");  
    								} else {
										echo "Logo Transparencia BNE";
    								}
								?>" class="img-responsive" />
								</span>
							</div>
						<h1 class="h1-inicio-museo" data-fontsize="45" data-lineheight="48">
							<span><?php 								
									// Carga el TITULO de "Visitas al Museo" (HOME)
									if (get_field("campo-titulo-transparencia-inicio") ) {
										the_field("campo-titulo-transparencia-inicio");
									}else{ 
										echo "Transparencia <br />BNE"; 
									}
								?></span>
						</h1>
						<div class="centrar-texto"><?php 								
								// Carga el texto descripcion de "Transparencia BNE" (HOME)
								if (get_field("campo-descripcion-transparencia-inicio") ) {
									the_field("campo-descripcion-transparencia-inicio");
								}else{ 
									echo "<p class='transparencia-bne'>La misión de la Biblioteca Nacional de España, desde hace más de 300 años, es la de preservar y transmitir el patrimonio bibliográfico y documental español. Su voluntad es revertir en la sociedad el inmenso legado cultural que atesora, potenciando al máximo el acceso, uso y reutilización de la información que conserva y genera, en beneficio de la sociedad. </p>";
									echo "<br />";
								}
							?>
						</div>
						<div class="fusion-clearfix"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="fusion-clearfix"></div>

			<div class="fusion-two-third fusion-layout-column fusion-spacing-no">
				<div class="fusion-column-wrapper">
					<div class="fusion-column-table">
						<div class="fusion-column-tablecell"><div class="fusion-clearfix"></div></div>
					</div></div>
				</div>
				<div class="fusion-one-third fusion-layout-column fusion-column-last fusion-spacing-no columna-derecha-modulo-transparencia">
					<div class="fusion-column-wrapper bordesup-visitar-museo">
						<div class="fusion-column-table">
							<div class="fusion-column-tablecell">
								<h2 class="h2-inicio-museo">
								<?php if( have_rows('campo-url-transparencia-inicio') ): ?>
									<?php while( have_rows('campo-url-transparencia-inicio') ): the_row(); 
										// variables
										$texto_enlace 	= get_sub_field('titulo-url-transparencia');
										$title_url 		= get_sub_field('title-url-transparencia');
										$enlace 		= get_sub_field('url-trasparencia');
										?>			
											<a class="h2-enlace-inicio-museo" title="<?php echo $title_url; ?>" href="<?php echo $enlace; ?>"><?php echo $texto_enlace; ?> &raquo;</a></h2>
									<?php endwhile; ?>
								<?php endif; ?>
								<div class="fusion-clearfix"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="fusion-clearfix"></div>

				<div class="fusion-two-third fusion-layout-column fusion-spacing-no">
					<div class="fusion-column-wrapper">
						<div class="fusion-column-table">
							<div class="fusion-column-tablecell">
								<div class="fusion-clearfix"></div>
							</div>
						</div>
					</div>
				</div>
		</div>
	</div>