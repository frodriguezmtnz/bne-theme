<?php
/**
 * Template Name: Sección Colecciones (Inicio BNE)
 * Esta plantilla se usa como modulo del template (BNE-Theme/inicio.php) 
 *   Carga los campos personalizados editables desde el panel de control de WP,
 *   más el diseño y programación para este módulo.
 *
 * @author 		Felipe Rodríguez (Serikat)
 * @package 	BNE-Theme/templates/bne/colecciones-inicio.php
 * @version     1.0
 */
?>

<?php

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php //get_header(); ?>

<!-- inicio colecciones -->
			<?php
			// CPT Loop & Show for Colecciones
			//******************************
			global $post;

			// The query arguments: https://codex.wordpress.org/Template_Tags/get_posts
			$args = array(
			    'post_type' => 'coleccion',
			    'posts_per_page' => 5,
			    'categoria' => 'tematica', //muestra solo las colecciones de esa categoria, 'categoria' => 'slug_category_taxonomy',
				/*'tax_query' => array(
					array(
						'taxonomy' => 'categoria',
						'field'    => 'tematica',
					),
				),*/
			    'order' => 'DESC',
			    'orderby' => 'RAND', // 'orderby' => 'rand',
			    'post_status' => 'publish',
			    'post' => array( $post->ID )
			    //'post__not_in' => array( $post->ID ) //todos menos el actual ID coleccion
			);

			// Create the related query
			$rel_query = new WP_Query( $args );

			// Check if there is any colecciones pages
			if( $rel_query->have_posts() ) : 
			?>

			<?php
			    // The Loop
			    while ( $rel_query->have_posts() ) :
			        $rel_query->the_post();
			?>
				<div class="fusion-one-half fusion-layout-column fusion-column-inner-bg-wrapper fusion-spacing-no" style="margin-top:0px;margin-bottom:0px;"><div class="fusion-column-wrapper" style="border: 1px solid rgb(255, 255, 255); height: auto; min-height: 437px;"><div class="fusion-column-table" style="height: 251px;"><div class="fusion-column-tablecell"><div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-one"><h2 class="title-heading-center-coleccion"><?php the_title() ?></h2></div><div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-three"><h3 class="title-heading-center-resumen-coleccion" data-fontsize="21" data-lineheight="24"><?php echo (get_the_excerpt()); ?></h3></div><div class="fusion-clearfix"></div></div></div></div><span class="fusion-column-inner-bg hover-type-zoomin"><a href="<?php the_permalink() ?>" title="Colección <?php the_title();?> - <?php bloginfo ('name'); ?>"><span class="fusion-column-inner-bg-image" style="background:url(<?php the_post_thumbnail_url(); ?>) left top no-repeat; -webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"></span></a></span></div>
			<?php
			    endwhile;
			?>

			<?php
			endif;			
			//fin loop colecciones
			// Reset the query
			wp_reset_query();
			?>

			<div class="fusion-one-half fusion-layout-column fusion-column-inner-bg-wrapper fusion-spacing-no" style="margin-top:0px;margin-bottom:0px;"><div class="fusion-column-wrapper" style="border: 1px solid rgb(255, 255, 255); height: auto; min-height: 437px;">
				<div class="fusion-column-table" style="height: 251px;">
					<div class="fusion-column-tablecell">
						<div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-one">
							<h2 class="title-heading-center-coleccion">Descubre todas nuestras colecciones &raquo;</h2>
						</div>
						<div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-three">
							<h3 class="title-heading-center-colecciones" data-fontsize="21" data-lineheight="24">Nuestras colecciones son una selección de materiales digitalizados y organizados por temática.</h3>
						</div>
						<div class="fusion-clearfix"></div>
					</div>
				</div>
			</div>
			<span class="fusion-column-inner-bg hover-type-zoomin"><a href="/colecciones/" title="Colecciones Digitalizadas de la <?php bloginfo ('name'); ?>"><span class="fondo-ver-mas-colecciones"></span></a></span></div>
		<div class="fusion-clearfix"></div>			