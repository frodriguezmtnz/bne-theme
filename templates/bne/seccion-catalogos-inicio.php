<?php
/**
 * Template Name: Sección Catalogos 4cols (Inicio BNE)
 * Esta plantilla se usa como modulo del template (BNE-Theme/inicio.php) 
 *   Carga los campos personalizados editables desde el panel de control de WP,
 *   más el diseño y programación para este módulo.
 *
 * @author 		Felipe Rodríguez (Serikat)
 * @package 	BNE-Theme/templates/bne/seccion-catalogos-inicio.php
 * @version     1.0
 */

?>

<div class="fusion-row">
	<div class="fusion-one-fourth fusion-layout-column fusion-column-inner-bg-wrapper fusion-spacing-no columna-inicio-tres">
		<div class="fusion-column-wrapper columna-catalogos-inicio">
			<div class="fusion-column-table">
				<div class="fusion-column-tablecell">
					<div class="imageframe-align-center">
						<span class="fusion-imageframe hover-type-none">
							<img src="<?php 
									// Carga el Icono para la Columna-1 de CATALOGOS HOME
									if(get_field("columna1-icono-catalogos-inicio")) {
    									the_field("columna1-icono-catalogos-inicio");  
    								} else {
										echo get_stylesheet_directory_uri().'/images/catalogo-autoridades-bne.png';
    								}
								?>" alt="<?php 
									// Carga la etiqueta ALT para el Icono de la Columna-1 de CATALOGOS HOME
									if(get_field("columna1-alt-icono-catalogos-inicio")) {
    									the_field("columna1-alt-icono-catalogos-inicio");  
    								} else {
										echo "Catálogo General BNE";
    								}
								?>" class="img-responsive-catalogos" />
						</span>
					</div>
					<h2 data-fontsize="28" data-lineheight="27"><?php 
						// Carga el Título para la Columna-1 de CATALOGOS HOME
						if(get_field("columna1-titulo-catalogos-inicio")) {
    						the_field("columna1-titulo-catalogos-inicio");  
    					} else {
							echo "Catálogo <br />General";
    					}
					?>
					</h2><?php
						// Carga el texto Descripción para la Columna-1 de CATALOGOS HOME
						if(get_field("columna1-descripcion-catalogos-inicio")) {
    						the_field("columna1-descripcion-catalogos-inicio");  
    					} else {
							echo "<p>Se pueden consultar las referencias bibliográficas de todos los documentos conservados en la Biblioteca.</p>";
    					}
					?>					
					<div class="fusion-clearfix"></div>
				</div>
			</div>
		</div>
		<span class="fusion-column-inner-bg hover-type-liftup">
			<a title="<?php 
					// Campo TITLE para la Columna-1 de CATALOGOS HOME
					if (get_field('columna1-title-enlace-catalogos-inicio') ) {
						the_field('columna1-title-enlace-catalogos-inicio');
					}else{ 
						echo "Catálogo General BNE"; 
					}
				?> - Biblioteca Nacional de España" href="<?php 
					// Campo ENLACE para la Columna-1 de CATALOGOS HOME
					if (get_field('columna1-enlace-catalogos-inicio') ) {
						the_field('columna1-enlace-catalogos-inicio');
					}else{ 
						echo "http://catalogo.bne.es/";
					}
				?>" > <span class="fusion-column-inner-bg-image"></span> </a>
		</span>
	</div>

	<div class="fusion-one-fourth fusion-layout-column fusion-column-inner-bg-wrapper fusion-spacing-no columna-inicio-tres">
		<div class="fusion-column-wrapper columna-catalogos-inicio">
			<div class="fusion-column-table">
				<div class="fusion-column-tablecell">
					<div class="imageframe-align-center">
						<span class="fusion-imageframe hover-type-none">
							<img src="<?php 
									// Carga el Icono para la Columna-2 de CATALOGOS HOME
									if(get_field("columna2-icono-catalogos-inicio")) {
    									the_field("columna2-icono-catalogos-inicio");  
    								} else {
										echo get_stylesheet_directory_uri().'/images/bdh-hispanica-bne.png';
    								}
								?>" alt="<?php 
									// Carga la etiqueta ALT para el Icono de la Columna-2 de CATALOGOS HOME
									if(get_field("columna2-alt-icono-catalogos-inicio")) {
    									the_field("columna2-alt-icono-catalogos-inicio");  
    								} else {
										echo "biblioteca digital hispanica bne";
    								}
								?>" class="img-responsive-catalogos" />
						</span>
					</div>
					<h2 data-fontsize="28" data-lineheight="27"><?php 
						// Carga el Título para la Columna-2 de CATALOGOS HOME
						if(get_field("columna2-titulo-catalogos-inicio")) {
    						the_field("columna2-titulo-catalogos-inicio");  
    					} else {
							echo "Biblioteca Digital Hispánica";
    					}
					?>
					</h2><?php
						// Carga el texto Descripción para la Columna-2 de CATALOGOS HOME
						if(get_field("columna2-descripcion-catalogos-inicio")) {
    						the_field("columna2-descripcion-catalogos-inicio");  
    					} else {
							echo "<p>La Biblioteca Digital Hispánica es la biblioteca digital de la BNE. Proporciona acceso libre y gratuito a miles de documentos digitalizados.</p>";
    					}
					?>
					<div class="fusion-clearfix"></div>
				</div>
			</div>
		</div>
		<span class="fusion-column-inner-bg hover-type-liftup">
			<a title="<?php
					// Campo TITLE para la Columna-2 de CATALOGOS HOME
					if (get_field('columna2-title-enlace-catalogos-inicio') ) {
						the_field('columna2-title-enlace-catalogos-inicio');
					}else{ 
						echo "Biblioteca Digital Hispánica BNE"; 
					}
				?> - Biblioteca Nacional de España" href="<?php
					// Campo ENLACE para la Columna-2 de CATALOGOS HOME
					if (get_field('columna2-enlace-catalogos-inicio') ) {
						the_field('columna2-enlace-catalogos-inicio');
					}else{ 
						echo "http://www.bne.es/es/Catalogos/BibliotecaDigitalHispanica/Inicio/index.html";
					}
				?>" ><span class="fusion-column-inner-bg-image"></span></a>
		</span>
	</div>

	<div class="fusion-one-fourth fusion-layout-column fusion-column-inner-bg-wrapper fusion-spacing-no columna-inicio-tres">
		<div class="fusion-column-wrapper columna-catalogos-inicio">
			<div class="fusion-column-table">
				<div class="fusion-column-tablecell">
					<div class="imageframe-align-center">
						<span class="fusion-imageframe hover-type-none">
							<img src="<?php 
									// Carga el Icono para la Columna-3 de CATALOGOS HOME
									if(get_field("columna3-icono-catalogos-inicio")) {
    									the_field("columna3-icono-catalogos-inicio");  
    								} else {
										echo get_stylesheet_directory_uri().'/images/hemeroteca-digital-bne.png';
    								}
								?>" alt="<?php 
									// Carga la etiqueta ALT para el Icono de la Columna-3 de CATALOGOS HOME
									if(get_field("columna3-alt-icono-catalogos-inicio")) {
    									the_field("columna3-alt-icono-catalogos-inicio");  
    								} else {
										echo "Hemeroteca Digital BNE";
    								}
								?>" class="img-responsive-catalogos" />
						</span>
					</div>
					<h2 data-fontsize="28" data-lineheight="27"><?php
						// Carga el Título para la Columna-3 de CATALOGOS HOME
						if(get_field("columna3-titulo-catalogos-inicio")) {
    						the_field("columna3-titulo-catalogos-inicio");  
    					} else {
							echo "Hemeroteca <br /> Digital";
    					}
					?>
					</h2><?php 
						// Carga el texto Descripción para la Columna-3 de CATALOGOS HOME
						if(get_field("columna3-descripcion-catalogos-inicio")) {
    						the_field("columna3-descripcion-catalogos-inicio");  
    					} else {
							echo "<p>Tiene como objetivo la consulta y difusión pública de la colección digital de prensa histórica española.</p>";
    					}
					?>					
					<div class="fusion-clearfix"></div>
				</div>
			</div>
		</div>
		<span class="fusion-column-inner-bg hover-type-liftup">
			<a title="<?php 
					// Campo TITLE para la Columna-3 de CATALOGOS HOME
					if (get_field('columna3-title-enlace-catalogos-inicio') ) {
						the_field('columna3-title-enlace-catalogos-inicio');
					}else{ 
						echo "Hemeroteca Digital de la BNE"; 
					}
				?> - Biblioteca Nacional de España" href="<?php 
					// Campo ENLACE para la Columna-3 de CATALOGOS HOME
					if (get_field('columna3-enlace-catalogos-inicio') ) {
						the_field('columna3-enlace-catalogos-inicio');
					}else{ 
						echo "http://hemerotecadigital.bne.es/index.vm";
					}
				?>" ><span class="fusion-column-inner-bg-image"></span></a>
		</span>
	</div>

	<div class="fusion-one-fourth fusion-layout-column fusion-column-inner-bg-wrapper fusion-column-last fusion-spacing-no columna-inicio-cuatro">
		<div class="fusion-column-wrapper columna-catalogos-inicio-4col">
			<div class="fusion-column-table">
				<div class="fusion-column-tablecell">
					<div class="imageframe-align-center">
						<span class="fusion-imageframe hover-type-none">
							<img src="<?php 
									// Carga el Icono para la Columna-4 de CATALOGOS HOME
									if(get_field("columna4-icono-catalogos-inicio")) {
    									the_field("columna4-icono-catalogos-inicio");  
    								} else {
										echo get_stylesheet_directory_uri().'/images/datos-bne-icono.png';
    								}
								?>" alt="<?php 
									// Carga la etiqueta ALT para el Icono de la Columna-4 de CATALOGOS HOME
									if(get_field("columna4-alt-icono-catalogos-inicio")) {
    									the_field("columna4-alt-icono-catalogos-inicio");  
    								} else {
										echo "Datos BNE Biblioteca Nacional de España";
    								}
								?>" class="img-responsive-catalogos" />
						</span>
					</div>
					<h2 data-fontsize="28" data-lineheight="27"><?php 
						// Carga el Título para la Columna-4 de CATALOGOS HOME
						if(get_field("columna4-titulo-catalogos-inicio")) {
    						the_field("columna4-titulo-catalogos-inicio");  
    					} else {
							echo "Datos <br />BNE.es";
    					}
					?>
					</h2><?php 
						// Carga el texto Descripción para la Columna-4 de CATALOGOS HOME
						if(get_field("columna4-descripcion-catalogos-inicio")) {
    						the_field("columna4-descripcion-catalogos-inicio");  
    					} else {
							echo "<p>Datos.bne.es propone al usuario un nuevo modo de acercarse a las colecciones y recursos de la Biblioteca Nacional de España.</p>";
    					}
					?>
					<div class="fusion-clearfix"></div>
				</div>
			</div>
		</div>
		<span class="fusion-column-inner-bg hover-type-liftup">
			<a title="<?php 
					// Campo TITLE para la Columna-4 de CATALOGOS HOME
					if (get_field('columna4-title-enlace-catalogos-inicio') ) {
						the_field('columna4-title-enlace-catalogos-inicio');
					}else{ 
						echo "Portal de Datos Bibliográficos de la BNE"; 
					}
				?> - Biblioteca Nacional de España" href="<?php 
					// Campo ENLACE para la Columna-4 de CATALOGOS HOME
					if (get_field('columna4-enlace-catalogos-inicio') ) {
						the_field('columna4-enlace-catalogos-inicio');
					}else{ 
						echo "http://datos.bne.es/";
					}
				?>" ><span class="fusion-column-inner-bg-image"></span></a>
		</span>
	</div>

	<div class="fusion-clearfix"></div>

	<div class="fusion-title title fusion-title-center">
		<div class="title-sep-container">			
		</div>
		<h2 class="title-heading-center">			
			<a href="/catalogos/" title="Ver todos los Catálogos de la Biblioteca Nacional España (BNE)">
				&lsaquo; VER TODOS LOS CATÁLOGOS &rsaquo;
			</a>
		</h2>
		<div class="title-sep-container">
		</div>
	</div>
</div>