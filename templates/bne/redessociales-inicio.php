<?php
/**
 * Template Name: Redes Sociales y Timeline Twitter BNE (Inicio BNE)
 * Esta plantilla se usa como modulo del template (BNE-Theme/inicio.php) 
 *   Carga los campos personalizados editables desde el panel de control de WP,
 *   más el diseño y programación para este módulo.
 *
 * @author 		Felipe Rodríguez (Serikat)
 * @package 	BNE-Theme/templates/bne/redessociales-inicio.php
 * @version     1.0
 */
?>

<?php 
// Inicializacion para libreria para detectar el user-agent de movil/tablet
require_once 'mobile-detect/Mobile_Detect.php';
$detect = new Mobile_Detect;

//Si es tablet/movil, ponemos el width al 100%
if ( $detect->isMobile() || $detect->isTablet() ){ ?>
	<!-- Timeline TW y Redes Sociales BNE Mobile/Tablet -->
	<div id="content" style="width: 100%;">
		<h2>Timeline de Twitter BNE:</h2>	
		<div id="content-twitter-mobile-tablet">	
			<a class="twitter-timeline" data-aria-polite="assertive" data-chrome="nofooter noheader" data-tweet-limit="" data-border-color="#e3e3e3" data-height="400" data-dnt="true" data-theme="light" href="https://twitter.com/BNE_biblioteca">Tweets por BNE_biblioteca</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
		</div>
		<div id="content-redes-sociales-mobile-tablet">
			<h3>Redes Sociales</h3>
				<?php // ACF Módulo HOME: Redes sociales
				if( have_rows('redes-sociales-home') ): ?>
				<div id="row">
					<div class="fusion-social-networks-wrapper">
				<?php //Recorremos los subcampos, mostrando con el icono + url de la red social
				    while ( have_rows('redes-sociales-home') ) : the_row();
						$nombre_icono_redsocial = get_sub_field('icono-red-social-bne-home');
						$url_redsocial = get_sub_field('url-red-social-bne-home');
						if ($nombre_icono_redsocial!="rss"){
							$title_redsocial = ucfirst($nombre_icono_redsocial);
						}else{
							$title_redsocial = strtoupper($nombre_icono_redsocial);
						}
					?>			
						<a title="La BNE en <?php echo $title_redsocial; ?>" class="fusion-social-network-icon fusion-<?php echo $nombre_icono_redsocial; ?> fusion-icon-<?php echo $nombre_icono_redsocial; ?>" style="color:#46494a;" href="<?php echo $url_redsocial; ?>"><span class="screen-reader-text"><?php echo $nombre_icono_redsocial; ?></span></a>
					<?php endwhile; ?>
				
				<?php 
				// Seccion para añadir una red social nueva si no existe en el listado:
				// compuesta por: icono + nombre + url de la nueva red social (16x16 hasta 256x256, optimizado el tamaño por css)
				if (have_rows("nueva-red-social-home")) {
					while ( have_rows('nueva-red-social-home') ) : the_row();
						$icono_redsocial = get_sub_field('icono-nueva-red-social');
						$nombre_icono_redsocial = get_sub_field('nombre-nueva-red-social');
						$url_redsocial = get_sub_field('url-nueva-red-social');
						if ($nombre_icono_redsocial!="rss"){
							$title_redsocial = ucfirst($nombre_icono_redsocial);
						}
				?>			
						<a title="La BNE en <?php echo $title_redsocial; ?>" class="fusion-social-network-icon" style="color:#46494a;" href="<?php echo $url_redsocial; ?>"><img class="icono-redsocial-nueva" src="<?php echo $icono_redsocial; ?>" alt="icono <?php echo $nombre_icono_redsocial; ?>" /></a>
					<?php endwhile; ?>
				<?php } //end_if-> nueva_red-social-home ?>
					</div>
				</div>
				<?php endif; ?>
		</div>
	</div>
<?php 
// Si no, es versión desktop, y ponemos 70% y 30% para las columnas.
}else{ ?>
	<!-- Timeline TW y Redes Sociales BNE Desktop -->
	<h2>Timeline de Twitter BNE:</h2>
	<div id="content" style="width: 100%; background-color: #66B4F0;">	
		<div id="content-twitter">	
			<a class="twitter-timeline" data-aria-polite="assertive" data-chrome="nofooter noheader" data-tweet-limit="" data-border-color="#e3e3e3" data-height="400" data-dnt="true" data-theme="light" href="https://twitter.com/BNE_biblioteca">Tweets por BNE_biblioteca</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
		</div>
		<div id="content-redes-sociales">
			<h3>Redes Sociales</h3>
				<?php // ACF Módulo HOME: Redes sociales
				if( have_rows('redes-sociales-home') ): ?>
				<div id="row">
					<div class="fusion-social-networks-wrapper">
				<?php //Recorremos los subcampos, mostrando con el icono + url de la red social
				    while ( have_rows('redes-sociales-home') ) : the_row();
						$nombre_icono_redsocial = get_sub_field('icono-red-social-bne-home');
						$url_redsocial = get_sub_field('url-red-social-bne-home');
						if ($nombre_icono_redsocial!="rss"){
							$title_redsocial = ucfirst($nombre_icono_redsocial);
						}else{
							$title_redsocial = strtoupper($nombre_icono_redsocial);
						}
					?>			
						<a title="La BNE en <?php echo $title_redsocial; ?>" class="fusion-social-network-icon fusion-<?php echo $nombre_icono_redsocial; ?> fusion-icon-<?php echo $nombre_icono_redsocial; ?>" style="color:#46494a;" href="<?php echo $url_redsocial; ?>"><span class="screen-reader-text"><?php echo $nombre_icono_redsocial; ?></span></a>
					<?php endwhile; //end_while-> redes-sociales-home ?>
				<?php 
				// Seccion para añadir una red social nueva si no existe en el listado:
				// compuesta por: icono + nombre + url de la nueva red social (16x16 hasta 256x256, optimizado el tamaño por css)
				if (have_rows("nueva-red-social-home")) {
					while ( have_rows('nueva-red-social-home') ) : the_row();
						$icono_redsocial = get_sub_field('icono-nueva-red-social');
						$nombre_icono_redsocial = get_sub_field('nombre-nueva-red-social');
						$url_redsocial = get_sub_field('url-nueva-red-social');
						if ($nombre_icono_redsocial!="rss"){
							$title_redsocial = ucfirst($nombre_icono_redsocial);
						}
				?>			
						<a title="La BNE en <?php echo $title_redsocial; ?>" class="fusion-social-network-icon" style="color:#46494a;" href="<?php echo $url_redsocial; ?>"><img class="icono-redsocial-nueva" src="<?php echo $icono_redsocial; ?>" alt="icono <?php echo $nombre_icono_redsocial; ?>" /></a>
					<?php endwhile; //end_while-> nueva_red-social-home ?>
				<?php } //end_if-> nueva_red-social-home ?>
					</div>
				</div>
				<?php endif; ?>
		</div>
	</div>
<?php }	//end_if (else) ?>