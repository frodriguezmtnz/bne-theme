<?php
/**
 * Template Name: Sección FAQs 4cols (Inicio BNE)
 * Esta plantilla se usa como modulo del template (BNE-Theme/inicio.php) 
 *   Carga los campos personalizados editables desde el panel de control de WP,
 *   más el diseño y programación para este módulo.
 *
 * @author 		Felipe Rodríguez (Serikat)
 * @package 	BNE-Theme/templates/bne/seccion-faqs-inicio.php
 * @version     1.0
 */


?>

<div class="fusion-row">
	<div class="fusion-one-fourth fusion-layout-column fusion-column-inner-bg-wrapper fusion-spacing-no columna-inicio-tres">
		<div class="fusion-column-wrapper columna-faqs-inicio">
			<div class="fusion-column-table">
				<div class="fusion-column-tablecell">
					<div class="imageframe-align-center">
						<span class="fusion-imageframe hover-type-none">
							<img src="<?php 
									// Carga el Icono para la Columna-1 de FAQs HOME
									if(get_field("columna1-icono-faqs-inicio")) {
    									the_field("columna1-icono-faqs-inicio");  
    								} else {
										echo "/wp-content/uploads/2017/02/faqs-bne-6.png";
    								}
								?>" alt="<?php 
									// Carga la etiqueta ALT para el Icono de la Columna-1 de FAQs HOME
									if(get_field("columna1-alt-icono-faqs-inicio")) {
    									the_field("columna1-alt-icono-faqs-inicio");  
    								} else {
										echo "Visitar la Biblioteca de España FAQ";
    								}
								?>" class="img-responsive-faqs" />
						</span>
					</div>
					<h2 data-fontsize="28" data-lineheight="27"><?php 
						// Carga el Título para la Columna-1 de FAQs HOME
						if(get_field("columna1-titulo-faqs-inicio")) {
    						the_field("columna1-titulo-faqs-inicio");  
    					} else {
							echo "¿Se puede visitar la Biblioteca de España?";
    					}
					?>
					</h2><?php 
						// Carga el texto Descripción para la Columna-1 de FAQs HOME
						if(get_field("columna1-descripcion-faqs-inicio")) {
    						the_field("columna1-descripcion-faqs-inicio");  
    					} else {
							echo "<p>Claro, apúntate a una de nuestras visitas guiadas, visitas exprés o a nuestras Jornadas de puertas abiertas.</p>";
    					}
					?>					
					<div class="fusion-clearfix"></div>
				</div>
			</div>
		</div>
		<span class="fusion-column-inner-bg hover-type-liftup">
			<a title="<?php 
					// Campo TITLE para la Columna-1 de FAQs HOME
					if (get_field('columna1-title-enlace-faqs-inicio') ) {
						echo the_field('columna1-title-enlace-faqs-inicio');
					}else{ 
						echo "¿Se puede visitar la Biblioteca de España?"; 
					}
				?> - Biblioteca Nacional de España" href="<?php
					// Campo ENLACE para la Columna-1 de FAQs HOME
					if (get_field('columna1-enlace-faqs-inicio') ) {
						the_field('columna1-enlace-faqs-inicio');
					}else{ 
						echo "/servicios/preguntas-frecuentes/";
					}
				?>" > <span class="fusion-column-inner-bg-image"></span> </a>
		</span>
	</div>

	<div class="fusion-one-fourth fusion-layout-column fusion-column-inner-bg-wrapper fusion-spacing-no columna-inicio-tres">
		<div class="fusion-column-wrapper columna-faqs-inicio">
			<div class="fusion-column-table">
				<div class="fusion-column-tablecell">
					<div class="imageframe-align-center">
						<span class="fusion-imageframe hover-type-none">
							<img src="<?php 
									// Carga el Icono para la Columna-2 de FAQs HOME
									if(get_field("columna2-icono-faqs-inicio")) {
    									the_field("columna2-icono-faqs-inicio");  
    								} else {
										echo "/wp-content/uploads/2017/02/faqs-bne-2.png";
    								}
								?>" alt="<?php 
									// Carga la etiqueta ALT para el Icono de la Columna-2 de FAQs HOME
									if(get_field("columna2-alt-icono-faqs-inicio")) {
    									the_field("columna2-alt-icono-faqs-inicio");  
    								} else {
										echo "necesitar carnet de la bne";
    								}
								?>" class="img-responsive-faqs" />
						</span>
					</div>
					<h2 data-fontsize="28" data-lineheight="27"><?php 
						// Carga el Título para la Columna-2 de FAQs HOME
						if(get_field("columna2-titulo-faqs-inicio")) {
    						the_field("columna2-titulo-faqs-inicio");  
    					} else {
							echo "¿Necesito tener el carnet de la BNE?";
    					}
					?>
					</h2><?php 
						// Carga el texto Descripción para la Columna-2 de FAQs HOME
						if(get_field("columna2-descripcion-faqs-inicio")) {
    						the_field("columna2-descripcion-faqs-inicio");  
    					} else {
							echo "<p>Solamente necesitarás el carné si necesitas consultar los fondos de la Biblioteca.</p>";
    					}
					?>
					<div class="fusion-clearfix"></div>
				</div>
			</div>
		</div>
		<span class="fusion-column-inner-bg hover-type-liftup">
			<a title="<?php 
					// Campo TITLE para la Columna-2 de FAQs HOME
					if (get_field('columna2-title-enlace-faqs-inicio') ) {
						the_field('columna2-title-enlace-faqs-inicio');
					}else{ 
						echo "¿Necesito tener el carnet de la BNE?"; 
					}
				?> - Biblioteca Nacional de España" href="<?php 
					// Campo ENLACE para la Columna-2 de FAQs HOME
					if (get_field('columna2-enlace-faqs-inicio') ) {
						the_field('columna2-enlace-faqs-inicio');
					}else{ 
						echo "/servicios/preguntas-frecuentes/";
					}
				?>" ><span class="fusion-column-inner-bg-image"></span></a>
		</span>
	</div>

	<div class="fusion-one-fourth fusion-layout-column fusion-column-inner-bg-wrapper fusion-spacing-no columna-inicio-tres">
		<div class="fusion-column-wrapper columna-faqs-inicio">
			<div class="fusion-column-table">
				<div class="fusion-column-tablecell">
					<div class="imageframe-align-center">
						<span class="fusion-imageframe hover-type-none">
							<img src="<?php 
									// Carga el Icono para la Columna-3 de FAQs HOME
									if(get_field("columna3-icono-faqs-inicio")) {
    									the_field("columna3-icono-faqs-inicio");  
    								} else {
										echo "/wp-content/uploads/2017/02/faqs-bne-4.png";
    								}
								?>" alt="<?php 
									// Carga la etiqueta ALT para el Icono de la Columna-3 de FAQs HOME
									if(get_field("columna3-alt-icono-faqs-inicio")) {
    									the_field("columna3-alt-icono-faqs-inicio");  
    								} else {
										echo "estudiar en las salas de la BNE";
    								}
								?>" class="img-responsive-faqs" />
						</span>
					</div>
					<h2 data-fontsize="28" data-lineheight="27"><?php 
						// Carga el Título para la Columna-3 de FAQs HOME
						if(get_field("columna3-titulo-faqs-inicio")) {
    						the_field("columna3-titulo-faqs-inicio");  
    					} else {
							echo "¿Puedo ir a estudiar a vuestras salas?";
    					}
					?>
					</h2><?php 
						// Carga el texto Descripción para la Columna-3 de FAQs HOME
						if(get_field("columna3-descripcion-faqs-inicio")) {
    						the_field("columna3-descripcion-faqs-inicio");  
    					} else {
							echo "<p>No, tan sólo podrás consultar los fondos de la colección de la Biblioteca.</p>";
    					}
					?>					
					<div class="fusion-clearfix"></div>
				</div>
			</div>
		</div>
		<span class="fusion-column-inner-bg hover-type-liftup">
			<a title="<?php 
					// Campo TITLE para la Columna-3 de FAQs HOME
					if (get_field('columna3-title-enlace-faqs-inicio') ) {
						the_field('columna3-title-enlace-faqs-inicio');
					}else{ 
						echo "¿Puedo ir a estudiar a vuestras salas?"; 
					}
				?> - Biblioteca Nacional de España" href="<?php 
					// Campo ENLACE para la Columna-3 de FAQs HOME
					if (get_field('columna3-enlace-faqs-inicio') ) {
						the_field('columna3-enlace-faqs-inicio');
					}else{ 
						echo "/servicios/preguntas-frecuentes/";
					}
				?>" ><span class="fusion-column-inner-bg-image"></span></a>
		</span>
	</div>

	<div class="fusion-one-fourth fusion-layout-column fusion-column-inner-bg-wrapper fusion-column-last fusion-spacing-no columna-inicio-cuatro">
		<div class="fusion-column-wrapper columna-faqs-inicio-4col">
			<div class="fusion-column-table">
				<div class="fusion-column-tablecell">
					<div class="imageframe-align-center">
						<span class="fusion-imageframe hover-type-none">
							<img src="<?php 
									// Carga el Icono para la Columna-4 de FAQs HOME
									if(get_field("columna4-icono-faqs-inicio")) {
    									the_field("columna4-icono-faqs-inicio");  
    								} else {
										echo "/wp-content/uploads/2017/02/faqs-bne-5.png";
    								}
								?>" alt="<?php 
									// Carga la etiqueta ALT para el Icono de la Columna-4 de FAQs HOME
									if(get_field("columna4-alt-icono-faqs-inicio")) {
    									the_field("columna4-alt-icono-faqs-inicio");  
    								} else {
										echo "llevarse un libro de la bne a casa";
    								}
								?>" class="img-responsive-faqs" />
						</span>
					</div>
					<h2 data-fontsize="28" data-lineheight="27"><?php 
						// Carga el Título para la Columna-4 de FAQs HOME
						if(get_field("columna4-titulo-faqs-inicio")) {
    						the_field("columna4-titulo-faqs-inicio");  
    					} else {
							echo "¿Puedo llevarme un libro a casa?";
    					}
					?>
					</h2><?php 
						// Carga el texto Descripción para la Columna-4 de FAQs HOME
						if(get_field("columna4-descripcion-faqs-inicio")) {
    						the_field("columna4-descripcion-faqs-inicio");  
    					} else {
							echo "<p>No, la BNE es una biblioteca de consulta. Sólo podrás consultar nuestros fondos viniendo a visitarnos.</p>";
    					}
					?>
					<div class="fusion-clearfix"></div>
				</div>
			</div>
		</div>
		<span class="fusion-column-inner-bg hover-type-liftup">
			<a title="<?php 
					// Campo TITLE para la Columna-4 de FAQs HOME
					if (get_field('columna4-title-enlace-faqs-inicio') ) {
						the_field('columna4-title-enlace-faqs-inicio');
					}else{ 
						echo "¿Puedo llevarme un libro a casa?"; 
					}
				?> - Biblioteca Nacional de España" href="<?php 
					// Campo ENLACE para la Columna-4 de FAQs HOME
					if (get_field('columna4-enlace-faqs-inicio') ) {
						the_field('columna4-enlace-faqs-inicio');
					}else{ 
						echo "/servicios/preguntas-frecuentes/";
					}
				?>" ><span class="fusion-column-inner-bg-image"></span></a>
		</span>
	</div>

	<div class="fusion-clearfix"></div>

	<div class="fusion-title title fusion-title-center">
		<div class="title-sep-container"></div>
		<h2 class="title-heading-center">			
			<a href="/servicios/preguntas-frecuentes/" title="Resuelve todas tus dudas en nuestra FAQ de la BNE">&lsaquo; RESUELVE AQUÍ TODAS TUS DUDAS &rsaquo; </a>
		</h2>
		<div class="title-sep-container"></div>
	</div>
</div>