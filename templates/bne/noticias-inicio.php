<?php
/**
 * Template Name: Sección Noticias Grid (Inicio BNE)
 * Esta plantilla se usa como modulo del template (BNE-Theme/inicio.php) 
 *   Carga los campos personalizados editables desde el panel de control de WP,
 *   más el diseño y programación para este módulo.
 *
 * @author 		Felipe Rodríguez (Serikat)
 * @package 	BNE-Theme/templates/bne/noticias-inicio.php
 * @version     1.0
 * 
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) { exit( 'Direct script access denied.' ); }

// Inicializacion para libreria para detectar el user-agent de movil/tablet
require_once 'mobile-detect/Mobile_Detect.php';
$detect = new Mobile_Detect;

	global $post;

	// The query arguments: https://codex.wordpress.org/Template_Tags/get_posts
	$args = array(
	    'post_type' => 'post',
	    'posts_per_page' => 7,
	    'category_name' => 'noticias',
	    'order' => 'DESC',
	    'orderby' => 'date', // 'orderby' => 'rand',
	    'post_status' => 'publish',
	    'post' => array( $post->ID )
	);

	// Create the related query
	$rel_query = new WP_Query( $args );

	// Check if there is any colecciones pages
	if( $rel_query->have_posts() ) : ?>

	<div class="fusion-blog-shortcode fusion-blog-shortcode-1 fusion-blog-archive fusion-blog-layout-grid-wrapper fusion-blog-pagination fusion-blog-no-images fusion-no-col-space">
		<div class="fusion-posts-container fusion-posts-container-pagination fusion-blog-layout-grid fusion-blog-layout-grid-4 isotope" data-pages="2" data-grid-col-space="0.0" style="margin: -0px -0px 0;">
	<?php // The Loop for news + design
		while ( $rel_query->have_posts() ) :
	    	$rel_query->the_post();
	?>
		<div id="post" class="fusion-post-grid post post type-post status-publish format-standard has-post-thumbnail hentry category-noticias">
		<a href="<?php the_permalink() ?>" title="<?php the_title();?> - <?php bloginfo ('name'); ?>">
			<div class="fusion-post-wrapper hover-type-liftup">
				<div class="fusion-post-content-wrapper">
					<div class="fusion-post-content post-content">
						<h2 class="entry-title" data-fontsize="30" data-lineheight="27">
							<?php // *Imprime el Titulo de la noticia segun el dispositivo
								// Si es version movil/tablet, recortamos el titulo a 30 caracteres.
								if ( $detect->isMobile() || $detect->isTablet() ){
									the_title_recortado ('', '...', true, '30');
									//echo " &raquo;";
								}else{
									//echo wp_trim_words(get_the_title(), 7, '...');
									the_title_recortado ('', '...', true, '55');
									//echo " &raquo;";
								}								
							?>
						</h2>
					</div>
					<div class="fusion-post-content-wrapper-resumen">
						<div class="fusion-post-content-container"><?php
							// *Imprime el resumen manual o del contenido segun el dispositivo 
							if ( $detect->isMobile() || $detect->isTablet() ){
								// version móvil
								if (has_excerpt()) { //extracto manual
									echo limitar_palabras(get_the_excerpt(), '6');
								}else { //extracto auto del content recortado
									echo limitar_palabras(get_the_excerpt(), '5');	
								} 
							}else{ //version desktop
								if (has_excerpt()) { //extracto manual
									echo limitar_palabras(get_the_excerpt(), '8');			
								}else { //extracto auto del content recortado
									echo limitar_palabras(get_the_excerpt(), '6');	
								}
							}
						?>					
						</div>
					</div>

				</div>
				<div class="fusion-clearfix"></div>
			</div>
		</a>
		</div> 
<?php
		endwhile;			
	endif;?>

		<div id="post" class="fusion-post-grid post post type-post status-publish format-standard has-post-thumbnail hentry category-noticias">
			<div class="fusion-post-wrapper" style="background-color: #F8F8F8; color: #000;">
				<div class="fusion-post-content-wrapper">
					<div class="fusion-post-content post-content">
						<h2 class="entry-title-ver-noticias">
							<a href="/noticias/" title="Ver todas las noticias de la <?php bloginfo ('name'); ?>">Ver todas las noticias de la BNE TEST &raquo;</a>
						</h2>
					</div>
				</div>
				<div class="fusion-clearfix"></div>
			</div>
		</div>
<?php
	wp_reset_query(); // Reset the query wp
/* Omit closing PHP tag to avoid "Headers already sent" issues. */	