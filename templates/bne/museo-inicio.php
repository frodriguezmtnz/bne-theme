<?php
/**
 * Template Name: Sección Visitar MUSEO (Inicio BNE)
 * Esta plantilla se usa como modulo del template (BNE-Theme/inicio.php) 
 *   Carga los campos personalizados editables desde el panel de control de WP,
 *   más el diseño y programación para este módulo.
 *
 * @author 		Felipe Rodríguez (Serikat)
 * @package 	BNE-Theme/templates/bne/museo-inicio.php
 * @version     1.0
 */
?>
	<div class="fusion-fullwidth fullwidth-box fusion-fullwidth-2  fusion-parallax-none hundred-percent-fullwidth" style="    
    border-color: #eae9e9;
    border-bottom-width: 0px;
    border-top-width: 0px;
    border-bottom-style: solid;
    border-top-style: solid;
    padding-bottom: 0px;
    padding-top: 0px;
    background-position: left top;
    background-repeat: no-repeat;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    background-image: url(<?php 
    			if(get_field("campo-visitas-museo-imagen-fondo-inicio")) {
    				the_field("campo-visitas-museo-imagen-fondo-inicio");  
    			} else {
    				//echo "/wp-content/themes/bne-theme/images/museo-bne-inicio.jpg";
    				echo get_stylesheet_directory_uri().'/images/museo-bne-inicio.jpg';
    			} 
    		?> );
	">
		<div class="fusion-row">
			<div class="fusion-two-third fusion-layout-column fusion-spacing-no">
			<div class="fusion-column-wrapper">
				<div class="fusion-column-table">
					<div class="fusion-column-tablecell">
						<div class="fusion-clearfix"></div></div>
					</div>
				</div>
			</div>
			<div class="fusion-one-third fusion-layout-column fusion-column-last fusion-spacing-no columna-museo-derecha-transparencia">
				<div class="fusion-column-wrapper">
					<div class="fusion-column-table">
						<div class="fusion-column-tablecell">
							<div class="imageframe-align-center">
								<span class="fusion-imageframe imageframe-none imageframe-2 hover-type-none"><img src="<?php 

									if(get_field("campo-visitas-museo-logo-inicio")) {
    									the_field("campo-visitas-museo-logo-inicio");  
    								} else {
										echo get_stylesheet_directory_uri().'/images/logo-museo-bne.png';						
    								}
								?>" alt="<?php 
									// Carga la etiqueta ALT del logo de "Visitas al Museo" (HOME)
									if(the_field("campo-visitas-museo-logo-inicio") && get_field("campo-visitas-museo-alt-logo-inicio")) {
    									the_field("campo-visitas-museo-alt-logo-inicio");  
    								} else {
										echo "Logo visitar el museo de la BNE";
    								}
								?>" class="img-responsive" />
								</span>
							</div>
						<h1 class="h1-inicio-museo" data-fontsize="45" data-lineheight="48">
							<span><?php 								
									// Carga el TITULO de "Visitas al Museo" (HOME)
									if (get_field("campo-visitas-museo-titulo-inicio") ) {
										the_field("campo-visitas-museo-titulo-inicio");
									}else{ 
										echo "Visitas al <br /> Museo"; 
									}
								?></span>
						</h1>
						<div class="centrar-texto"><?php 								
								// Carga el texto descripcion de "Visitas al Museo" (HOME)
								if (get_field("campo-visitas-descripcion-museo-inicio") ) {
									the_field("campo-visitas-descripcion-museo-inicio");
								}else{ 
									echo "<p>De martes a sábado de 10 a 20 h.<br />
										  Domingos y festivos de 10 a 14 h.</p>"; 
								}
							?>
						</div>
						<div class="fusion-clearfix"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="fusion-clearfix"></div>

			<div class="fusion-two-third fusion-layout-column fusion-spacing-no">
				<div class="fusion-column-wrapper">
					<div class="fusion-column-table">
						<div class="fusion-column-tablecell"><div class="fusion-clearfix"></div></div>
					</div></div>
				</div>
				<div class="fusion-one-third fusion-layout-column fusion-column-last fusion-spacing-no columna-museo-derecha-transparencia">
					<div class="fusion-column-wrapper bordesup-visitar-museo">
						<div class="fusion-column-table">
							<div class="fusion-column-tablecell">
								<h2 class="h2-inicio-museo">
								<?php 
								// Campo ACF 'Links Exposiciones Actuales'
								if( have_rows('campo-visitas-museo-links-exposiciones-inicio') ): ?>
									<?php while( have_rows('campo-visitas-museo-links-exposiciones-inicio') ): the_row(); 
										// variables
										$texto_enlace 	= get_sub_field('texto');
										$title_url 		= get_sub_field('title-url');
										$enlace 		= get_sub_field('url');
										?>
										<a class="h2-enlace-inicio-museo" title="<?php echo $title_url; ?>" href="<?php echo $enlace; ?>"><?php echo $texto_enlace; ?> &raquo;</a></h2>
									<?php endwhile; ?>
								<?php endif; ?>
								<div class="fusion-clearfix"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="fusion-clearfix"></div>

				<div class="fusion-two-third fusion-layout-column fusion-spacing-no">
					<div class="fusion-column-wrapper">
						<div class="fusion-column-table">
							<div class="fusion-column-tablecell">
								<div class="fusion-clearfix"></div>
							</div>
						</div>
					</div>
				</div>

				<div class="fusion-one-third fusion-layout-column fusion-column-last fusion-spacing-no columna-museo-derecha-transparencia">
					<div class="fusion-column-wrapper bordesup-visitar-museo">
						<div class="fusion-column-table">
							<div class="fusion-column-tablecell">
								<h2 class="h2-inicio-museo">
								<?php 
								// Campo ACF 'Links Exposiciones Actuales'
								if( have_rows('campo-visitas-museo-links-visita-guiada') ): ?>
									<?php while( have_rows('campo-visitas-museo-links-visita-guiada') ): the_row(); 
										// variables
										$texto_enlace 	= get_sub_field('texto');
										$title_url 		= get_sub_field('title-url');
										$enlace 		= get_sub_field('url');
										?>
										<a class="h2-enlace-inicio-museo" title="<?php echo $title_url; ?>" href="<?php echo $enlace; ?>"><?php echo $texto_enlace; ?> &raquo;</a></h2>
									<?php endwhile; ?>
								<?php endif; ?>
							<div class="fusion-clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			<div class="fusion-clearfix"></div>
		</div>
	</div>