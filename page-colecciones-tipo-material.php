<?php
/**
 * Template Name: Colecciones Tipo de Material BNE (nivel 1)
 * Este plantilla se usa para las colecciones de tipo de material que tiene la BNE.es de nivel 1.
 *
 * @author 	Felipe Rodríguez (Serikat)
 * @package @package 	BNE-Theme/page-colecciones-tipo-material.php
 * @version 1.0
 */

?>

<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>
<div id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>
<p>Page colecciones tipo de material custom</p>

<!-- inicio 2 columnas izda -->
<div class="fusion-builder-row fusion-row "><div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2" style="margin-top:0px;margin-bottom:0px;width:100%; /*width:calc(50% - ( ( 4% ) * 0.5 ) );*/ margin-right: 0.2%;">
			<div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
				<div class="fusion-builder-row fusion-builder-row-inner fusion-row "><div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first fusion-spacing-no fusion-one-half fusion-column-first fusion-spacing-no 1_2" style="margin-top: 0px;margin-bottom: 0px;width:50%;width:calc(50% - ( ( 0 ) * 0.5 ) );margin-right:0px;">
				<a href="/colecciones/colecciones-destacadas/" title="Colecciones Destacadas - <?php bloginfo ('name'); ?>">
				<div class="fusion-column-wrapper" style="background-color:#262626;border-width: 1px;border-color: #a8a8a8;border-style: solid;padding: 40px 0px 40px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
					<div class="fusion-column-content-centered">
						<div class="fusion-column-content"><h2 style="color: #ffffff; text-align: center;" data-fontsize="28" data-lineheight="42">Ver <br> Colecciones destacadas</h2><p style="color: #8b8b8b; text-align: center;">Las mejores obras,<br> ordenadas por intereses</p>
						</div>
					</div>
				</div>
				</a>
			</div>
			<div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last fusion-spacing-no fusion-one-half fusion-column-last fusion-spacing-no 1_2" style="margin-top: 0px;margin-bottom: 0px;width:50%;width:calc(50% - ( ( 0 ) * 0.5 ) );">
			<div class="fusion-column-wrapper" style="background-color:#262626;border-width: 1px;border-color: #a8a8a8;border-style: solid;padding: 40px 0px 40px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
				<div class="fusion-column-content-centered"><div class="fusion-column-content"><h2 style="color: #8b8b8b; text-align: center;" data-fontsize="28" data-lineheight="42">Estás viendo Colecciones<br> por tipo de material</h2>
					<p style="color: #8b8b8b; text-align: center;">Manuscritos, cartografía,<br> archivos sonoros...</p>
				</div></div>
			</div>
		</div></div><div class="fusion-builder-row fusion-builder-row-inner fusion-row "><div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last fusion-one-full fusion-column-first fusion-column-last 1_1" style="margin-top: 0px;margin-bottom: 0px;">
			<div class="fusion-column-wrapper" style="background-color:#262626;border-width: 1px;border-color: #ffffff;border-style: solid;padding: 40px 0px 40px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
				<!-- buscador shortcode -->
				<?php echo do_shortcode('[wpdreams_ajaxsearchpro id=3]'); ?>
			</div>
		</div></div><div class="fusion-clearfix"></div>
		<!-- inicio colecciones tematicas -->
			<?php
			// CPT Loop & Show for Colecciones Tipo de Material			
			//*************************************************
			global $post;
			
			// The query arguments: https://codex.wordpress.org/Template_Tags/get_posts
			$args = array(
				'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
			    'post_type' => 'coleccion',
			    'posts_per_page' => 4,
			    // NO mostramos la categoría de término 'temática', si no todas las demás
			    'tax_query' => array(
			    		'taxonomy' => 'categoria',
			    		'field' => 'slug',
			    		'terms' => 'tematica',
			    		'operator' => 'NOT IN'
			    	),
			    'order' => 'ASC',
			    'orderby' => 'date', // 'orderby' => 'rand',
			    'post_status' => 'publish',
			    'post' => array( $post->ID )			    
			);

			// Create the related query
			$rel_query = new WP_Query( $args );

			// Check if there is any colecciones pages
			if( $rel_query->have_posts() ) : 
			
			// The Loop
			   	while ( $rel_query->have_posts() ) :
			        $rel_query->the_post();
			?>
				<div class="fusion-one-half fusion-layout-column fusion-column-inner-bg-wrapper fusion-spacing-no" style="margin-top:0px;margin-bottom:0px;"><div class="fusion-column-wrapper" style="border: 1px solid rgb(255, 255, 255); height: auto; min-height: 437px;"><div class="fusion-column-table" style="height: 251px;"><div class="fusion-column-tablecell"><div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-one"><h2 class="title-heading-center-coleccion"><?php the_title() ?></h2></div><div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-three"><h3 class="title-heading-center" data-fontsize="21" data-lineheight="24"><?php the_excerpt(); ?></h3></div><div class="fusion-clearfix"></div></div></div></div><span class="fusion-column-inner-bg hover-type-zoomin"><a href="<?php the_permalink() ?>" title="<?php the_title();?> - <?php bloginfo ('name'); ?>"><span class="fusion-column-inner-bg-image" style="background:url(<?php the_post_thumbnail_url(); ?>) left top no-repeat; -webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"></span></a></span></div>
			<?php
			    endwhile;
			endif;

			//Llamada al shortcode que hace la magia del ajax para cargar todos los CPTs de colecciones, de 4 en 4, de todas las categorías menos de 'tematica'
			echo do_shortcode ("[ajax_load_more id='loadmore_colecciones' container_type='div' post_type='coleccion' post_format='standard' taxonomy='categoria' taxonomy_terms='tematica' taxonomy_operator='NOT IN' pause='true' scroll='false' posts_per_page='4' transition='fade' progress_bar_color='ed7070' button_label='Cargar más colecciones' button_loading_label='Cargando colecciones...' destroy_after='1']");
			
			// Reset the query
			wp_reset_query();
			?>
<!-- fin loop colecciones -->			
		<div class="fusion-clearfix"></div>			
		</div></div>
<!-- fin 2 columnas izda -->
</div>
<?php //do_action( 'avada_after_content' ); ?>
<?php get_footer();
/* Omit closing PHP tag to avoid "Headers already sent" issues. */