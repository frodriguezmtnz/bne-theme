<?php
/**
 * Template Name: Noticias BNE (nivel 1)
 * Este plantilla se usa para la sección Noticias que tiene la BNE.es (nivel 1).
 *
 * @author 	Felipe Rodriguez Serikat
 * @package @package 	BNE-Theme/page-noticias.php
 * @version 1.0
 */

?>

<?php

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>
<div id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>

<div id="main" class="clearfix " style="">
<div class="fusion-row" style="">
	<div id="content" style="width: 100%;">
		<div id="post-531" class="post-531 page type-page status-publish hentry">
			
		<div class="post-content">
			<div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth" style="background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:0px;padding-bottom:20px;padding-left:0px;">
			<div class="fusion-builder-row fusion-row ">
				<div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-first 1_4" style="margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 4% ) * 0.25 ) );margin-right: 4%;">
					<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">

<!-- contenido columna izda noticias -->			
				<p><strong>ÁREA DE PRENSA</strong></p>
<p><span style="color: #52abe9;">MATERIAL GRÁFICO</span></p>
<p><span style="color: #52abe9;">DOSSIERES DE PRENSA</span></p>
<p><span style="color: #52abe9;">CONVOCATORIAS DE PRENSA</span></p>
<p><span style="color: #52abe9;">INFORMACIÓN INSTITUCIONAL</span></p>
<p><span style="color: #52abe9;">ALQUILER DE ESPACIOS</span></p>
<p><strong>Contacto:</strong></p>
<p>91 516 80 60<br>
91 516 80 17<br>
91 516 80 23<br>
comunicacion.bne@bne.es</p>
<p>Para acreditaciones escriba a:<br>
gabinete.prensa@bne.es</p>
<div class="fusion-clearfix"></div>

					</div>
				</div>

				<div class="fusion-layout-column fusion_builder_column fusion_builder_column_3_4  fusion-three-fourth fusion-column-last 3_4" style="margin-top:0px;margin-bottom:0px;width:75%;width:calc(75% - ( ( 4% ) * 0.75 ) );">
					<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
						<div class="fusion-blog-shortcode fusion-blog-shortcode-1 fusion-blog-archive fusion-blog-layout-grid-wrapper fusion-blog-infinite fusion-blog-no-images fusion-no-col-space">
							<style type="text/css">
									.fusion-blog-shortcode-1 .fusion-blog-layout-grid .fusion-post-grid{padding:0px;}.fusion-blog-shortcode-1 .fusion-posts-container{margin-left: -0px !important; margin-right:-0px !important;}
							</style>
							<div class="fusion-posts-container fusion-posts-container-infinite fusion-posts-container-load-more fusion-blog-layout-grid fusion-blog-layout-grid-3 isotope" data-pages="2" data-grid-col-space="0.0" style="margin: 0px; height: 600px; position: relative;">


						<?php 
							get_template_part( 'templates/bne/layout', 'noticias' ); 
							//get_template_part( 'templates/bne/noticias', 'inicio' );
							//echo do_shortcode("[fusion_blog]"); 
							//echo do_shortcode("[fusion_blog_bne]"); 
							//get_template_part( 'templates/blog', 'layout' );

							//echo do_shortcode ("[ajax_load_more id='cargar-mas-noticias' container_type='div' post_type='post' posts_per_page='4' category='noticias' pause='true' transition='fade' button_label='Cargar más noticias' button_loading_label='Cargando el resto de noticias...']");	
						?>

							</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
		</div>
	</div>
	</div>
</div>
</div>
</div>

<?php //do_action( 'avada_after_content' ); ?>
<?php get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */