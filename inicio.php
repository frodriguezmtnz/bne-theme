<?php
/**
 * Template Name: Inicio BNE.es
 * Este plantilla se usa para la página de Inicio para la nueva web de la BNE.es
 *
 * @author 	Felipe Rodríguez (Serikat)
 * @package @package 	BNE-Theme/inicio.php
 * @version 1.0
 */

?>

<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>

<?php get_header(); ?>
<div class="fusion-row">
	<!-- Slider BNE Inicio -->
	<div id="content" style="width: 100%;">
		<?php 		
			//LLamada al shortcode del slider + mostrar/ocultar modulo
			if (!get_field("mostrar-ocultar-modulo-slider-inicio")){
				// 100% OK: Checking if shortcode for slider has_content (not empty). Then, show it up!
				$slider = do_shortcode("[metaslider id=943]"); 
				if (!empty($slider))
					{ echo $slider;  }
			}
		?>			
	</div>
	<!-- Eventos BNE Inicio -->
	<div id="content" style="width: 100%;">
		<?php //LLamada al template del modulo Colecciones + mostrar/ocultar modulo
			if (!get_field("mostrar-ocultar-modulo-eventos-inicio")){
				echo do_shortcode("[fusion_eventos_inicio]"); 		
			}
		?>
	</div>
	<!-- Colecciones BNE Inicio -->
	<div id="content" style="width: 100%;">
		<?php //LLamada al template del modulo Colecciones + mostrar/ocultar modulo
			if (!get_field("mostrar-ocultar-modulo-colecciones-inicio")){
				get_template_part( 'templates/bne/colecciones', 'inicio' ); 		
			}
		?>
	</div>
	<!-- Noticias BNE Inicio -->
	<div id="content" style="width: 100%;">
		<?php //LLamada al template del modulo Noticias + mostrar/ocultar modulo
			if (!get_field("mostrar-ocultar-modulo-noticias-inicio")){
				get_template_part( 'templates/bne/noticias', 'inicio' ); 		
			}
		?>
	</div>
	<!-- Visitas Museo BNE Inicio -->
	<div id="content" style="width: 100%;">
		<?php //LLamada al template del modulo Museo + mostrar/ocultar modulo
			if (!get_field("mostrar-ocultar-modulo-visitar-museo-inicio")){
				get_template_part( 'templates/bne/museo', 'inicio' ); 		
			}
		?>
	</div>
	<!-- Catalogos BNE Inicio -->
	<div id="content" style="width: 100%;">
		<?php //LLamada al template del modulo Catálogos + mostrar/ocultar modulo
			if (!get_field("mostrar-ocultar-modulo-catalogos-inicio")){
				get_template_part( 'templates/bne/seccion-catalogos', 'inicio' );		
			}
		?>
	</div>
	<!-- Sección Transparencia BNE -->
	<div id="content" style="width: 100%;">
		<?php //LLamada al template del modulo Transparencia + mostrar/ocultar modulo
			if (!get_field("mostrar-ocultar-modulo-transparencia-inicio")){
				get_template_part( 'templates/bne/transparencia', 'inicio' ); 		
			}
		?>
	</div>	
	<!-- Redes sociales y Timeline TW BNE -->
	<?php //Muestra o no el timeline de twitter + redes social según módulo
    if(!get_field("mostrar-ocultar-modulo-twitter-inicio")) {
		get_template_part( 'templates/bne/redessociales', 'inicio' ); 		
    } ?>
	<!-- Sección Bibliotecarios BNE -->
	<div id="content" style="width: 100%;">
		<?php //LLamada al template del modulo Bibliotecarios + mostrar/ocultar modulo
			if (!get_field("mostrar-ocultar-modulo-bibliotecarios-inicio")){
			get_template_part( 'templates/bne/bibliotecarios', 'inicio' );		
			}
		?>
	</div>
	<!-- FAQs BNE Inicio -->
	<div id="content" style="width: 100%;">
		<?php //LLamada al template del modulo FAQs + mostrar/ocultar modulo
			if (!get_field("mostrar-ocultar-modulo-faqs-inicio")){
				get_template_part( 'templates/bne/seccion-faqs', 'inicio' );		
			}
		?>
	</div>
	<!-- BNE Labs BNE Inicio -->
	<div id="content" style="width: 100%;">
		<?php //LLamada al template del modulo BNE-Labs + mostrar/ocultar modulo
			if (!get_field("mostrar-ocultar-modulo-bnelabs-inicio")){
				get_template_part( 'templates/bne/bnelabs', 'inicio' );		
			}
		?>
	</div>	
</div><!-- fusion-row -->
</div><!-- antes footer -->
<?php do_action( 'avada_after_content' ); ?>
<?php get_footer();
/* Omit closing PHP tag to avoid "Headers already sent" issues. */