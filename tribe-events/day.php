<?php
/**
 * Day View Template
 * The wrapper template for day view.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/day.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

do_action( 'tribe_events_before_template' );
?>

<!-- Tribe Bar -->
<?php tribe_get_template_part( 'modules/bar' ); ?>

<h3><a href="<?php echo tribe_get_upcoming_link() ?>" title="Próximos Eventos en la BNE">Próximos Eventos</a>
&nbsp; | &nbsp; <a href="http://10.6.217.104/calendario/mes/" title="Calendario de Eventos de la BNE">Calendario de Eventos</a></h3>

<!-- Main Events Content -->
<?php tribe_get_template_part( 'day/content' ) ?>

<div class="tribe-clear"></div>


<?php //echo do_shortcode('[tribe_events view="month"]'); ?>


<!-- Calendario Eventos BNE
<h1>TESTING CALENDARIO day.php</h1>	 -->
<?php

//tribe_get_template_part( 'modules/bar' );
/* Codigo para mostrar calendario actual */
//$now = date_i18n( 'Y-m-01' ); //get the date so we show the right month
//tribe_show_month( array( 'eventDate' => $now, 'tribe-events/month/content' ) ); //get the events for the month

/*
$now = date_i18n( 'Y-m-01' ); //get the date so we show the right month
echo tribe_events_before_html();
tribe_show_month( array( 'eventDate' => $now ) ); //get the events for 
echo tribe_events_after_html();
*/

//echo do_shortcode('[tribe_events view="month"]'); ?>

<?php 
do_action( 'tribe_events_after_template' );
