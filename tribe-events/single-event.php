<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural = tribe_get_event_label_plural();

$event_id = get_the_ID();

?>

<script type="text/javascript">

$('article').readmore({
  speed: 75,
  moreLink: '<button class="btn-custom">LEER TEXTO DEL EVENTO</button>',
  lessLink: '<button class="btn-custom">LEER MENOS</button>'
  afterToggle: function(trigger, element, expanded) {
    if(! expanded) { // The "Close" link was clicked
      $('html, body').animate( { scrollTop: element.offset().top }, {duration: 100 } );
    }
  }
});

var moreText = "Leer más",
    lessText = "Leer menos",
    moreButton = $("button.btn-custom");

moreButton.click(function () {
    var $this = $(this);
    $this.text($this.text() == moreText ? lessText : moreText).next(".content-expand").slideToggle("fast");
});

</script>

<div id="tribe-events-content" class="tribe-events-single vevent hentry">

	<!-- Notices -->
	<?php
	if ( function_exists( 'tribe_the_notices' ) ) {
		tribe_the_notices();
	} else {
		tribe_events_the_notices();
	}
	?>

	<?php while ( have_posts() ) :  the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php if ( has_post_thumbnail() ) :  ?>
				<div class="fusion-events-featured-image">
					<div class="hover-type-<?php echo Avada()->settings->get( 'ec_hover_type' ); ?>">
						<!-- Event featured image, but exclude link -->						
						<?php echo tribe_event_featured_image( $event_id, 'full', false ); ?>

						<?php Avada_EventsCalendar::render_single_event_title(); ?>
					</div>
			<?php else : //Para mostrar la Imagen Sin eventos (si no hay imagen destacada) ?>
				<div class="fusion-events-featured-image fusion-events-single-title">

					<img class="attachment-full size-full wp-post-image" src="<?php echo get_stylesheet_directory_uri().'/images/no-imagen-eventos-bne-evento-blanco.png' ?>" alt="<?php get_the_title() ?>" />
					<?php Avada_EventsCalendar::render_single_event_title(); ?>
			<?php endif; ?>
				</div>

			<!-- Event content -->
			<?php do_action( 'tribe_events_single_event_before_the_content' ); ?>
			<div class="tribe-events-single-event-description tribe-events-content entry-content description"><?php the_content(); ?>
			<?php // Toogle Leer Mas para single Coleccion 
			// Siempre y cuando esté relleno el custom field llamado "leer-mas-coleccion"
			// Se muestra el botón + efecto fade
			// Si está vacío, NO se muestra el botón
			if (get_field("leer-mas-evento")){ ?>
				<div class="content-expand" style="display: none;">
					<?php the_field("leer-mas-evento"); ?>
				</div>
				<button class="btn-custom">LEER TEXTO DEL EVENTO</button>
			<?php } ?>
			</div>
			<!-- .tribe-events-single-event-description -->
			<?php do_action( 'tribe_events_single_event_after_the_content' ); ?>

			<!-- Event meta -->
			<?php do_action( 'tribe_events_single_event_after_the_meta' ); ?>
		</div> <!-- #post-x -->
		<?php

		avada_render_social_sharing( 'events' );

		?>
		<?php
		if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) {

			add_filter( 'comments_template', 'add_comments_template' );

			function add_comments_template() {
				return Avada::$template_dir_path . '/comments.php';
			}

			comments_template();
		}
		?>
	<?php endwhile;
	?>

	<!-- Event footer -->
	<div id="tribe-events-footer">
		<!-- Navigation -->
		<h3 class="tribe-events-visuallyhidden"><?php printf( __( '%s Navigation', 'the-events-calendar' ), $events_label_singular ); ?></h3>
		<ul class="tribe-events-sub-nav">
			<li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link( '%title%' ) ?></li>
			<li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '%title%' ) ?></li>
		</ul>
		<!-- .tribe-events-sub-nav -->
	</div>
	<!-- #tribe-events-footer -->

</div><!-- #tribe-events-content -->
