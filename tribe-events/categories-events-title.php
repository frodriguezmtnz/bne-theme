<?php
// Prepends category name(s) to the event titles
function tribe_events_title_include_cat ($title, $id) {

	$separator = ' &raquo; '; // HTML Separator between categories and title
	$cats = get_the_terms($id, 'tribe_events_cat');
	$is_ajax = defined('DOING_AJAX') && DOING_AJAX;
	$is_truly_admin = is_admin() && !$is_ajax;

	if (tribe_is_event($id) && $cats && !is_single() && !$is_truly_admin) {
		$cat_titles = array();
		foreach($cats as $i) {
			$cat_titles[] = $i->name;
		}
		$title = implode(', ', $cat_titles) . $separator . $title;
	}

	return $title;
}
add_filter('the_title', 'tribe_events_title_include_cat', 100, 2); 
?>